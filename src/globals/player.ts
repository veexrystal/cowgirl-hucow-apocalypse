export const PROJECTS = ['combat', 'defense', 'utility'] as const
export type TProjects = typeof PROJECTS[number]

export enum GENDER {
	MALE = 0,
	FEMALE = 1,
}
