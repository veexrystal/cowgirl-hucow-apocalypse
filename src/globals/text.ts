export const TEXT_TRANSLATION: any = {
	INFO: {
		TYPE: {
			FURRY: `Cowgirl`,
			HUCOW: `Hucow`,
		},
		COWHUMANOID: {
			FURRY: `Cow Humanoid`,
			HUCOW: `Hucow`,
		},
		COWBEASTS: {
			FURRY: `cowbeasts`,
			HUCOW: `hucows`,
		},
		BOVINE: {
			FURRY: `Bovine`,
			HUCOW: `Hucow`,
		},
		TAURS: {
			FURRY: `Cowtaur`,
			HUCOW: `Hucow`,
		},
		COW: {
			FURRY: `Cow`,
			HUCOW: `Hucow`,
		},
	},
	DETAIL: {
		SKIN: {
			FURRY: `fur`,
			HUCOW: `skin`,
		},
		FACE: {
			FURRY: 'muzzle',
			HUCOW: 'face',
		},
	},
}

interface ICowGirlText {
	DESCRIPTION: string
	WINTEXT:
		| {
				MALE: string
				FEMALE: string
		  }
		| string
	LOSETEXT:
		| {
				MALE: string
				FEMALE: string
		  }
		| string
}

interface IInfectionText {
	MALE: {
		DESCRIPTION: string[]
		PROGRESS: string[]
	}
	FEMALE: {
		DESCRIPTION: string[]
		PROGRESS: string[]
	}
	ENDING: number
	HUCOWENDING?: number
}

export const TEXT: any = {
	INTRODUCTION: `The day started like any other. A typical day where you woke up in your bed, in your apartment ready to go to work. Turning on the TV only briefly to the signs of "Breaking news! Military declares martial law. Warning citizens not leave their homes or approach anyone and wait for the army. More information slowly trickled in, reports of strange "$translation:Humanlike beasts|Large chested human females$" and "Cowlike humans" that roamed around the cities, as the information slowly piled up the picture of what was truly happening grew clearer.
	
	It wasn't until a dashcam recording showed a police officer trying to arrest one of these strange beasts that the true horror of what was happening was uncovered. The video showed a policeman pulling out his firearm, slowly approaching what looked to be a human female in shape. $translation:Covered in white fur|Skin a pristine, blemishless smooth pink$, her chest home to two extraordinarily large $translation:fur covered|E cup$ breasts, constantly jiggling as inch long teats for nipples leaked a steady stream of what was most likely milk. Its head $translation:only barely resembled a humans at this point. Other than overall structure, the ears were on the side of its oval head, two eyes above a slightly protruding bovine muzzle and human hair (a bright sky blue in this case) the fur along with its two stubby horns sticking out between the locks of hair. Its ears too were quite different, instead of stubby round human ears, they were long thin leathery ears, colour matching the fur on her body|a sensual feminine face, large lips, smooth full cheeks, long lashes and silky hair, looking like a supermodel. The only strange feature it had were two stubby horns sticking out between the locks of hair, bovine in appearance$. It turned to face the policeman stepping towards it. Breasts jiggled up and down as milk escaped its confines. He shot his taser at the beast. It jolted a little but soon returned back to its moseying walk towards the policeman. It eventually reached him and pinned him to the ground out of the camera's view. Nothing could be seen, but after a couple minutes another $translation:info.type$ and the original one appeared from the floor where she had tackled him. The new $translation:info.type$ was similar to the first. Her $translation:fur however was black and white and|body blemishless and pink and$ and chest was smaller (yet leaked the same amount of milk)$translation:| and between her hair was those telltale stubby horns$. The major difference was the new $translation:info.type$ was wearing the tattered remains of a policeman's uniform. They both then wandered off together. Both fondling each other breasts as they walked off down the street stubby tails wafting behind their large bouncy rears.
	
	Needless to say the video caused quite the outrage. People claiming it to be a prank, a CGI of some fetish gone too far. But other videos soon surfaced showing the same. Some showed the "Victim" changing entirely from a human into a large breasted $translation:info.cowhumanoid$. All of them showed the same series of events though. First one of the $translation:info.type$s corners a victim (or surprises them) pins them down, thrusts their large milky breasts in their mouth. At first the Human resists but once milk enters their mouth they start to drink greedily. Changes wrack their body as fur grows, Chests grow (or gain in size depending on gender) as a muzzle forms. In less than minute in some cases they are a new bovine creature able to spread their milk (and more than willing to) to anyone they come across, be it human or more of their new kind. These were deemed "Mooers" by the community due to their docile cow like tendencies as well as their copious amounts of mooing. Either as a mean of affection, pleasure or communication no one quite knew.
	
	An official statement came from the Army about two hours after the Dashcam footage was taken. It was vague and unhelpful. The piece of actual information it gave was "Don't drink the milk". This caused quite the stir around the city. Did they mean the milk from the $translation:info.cowbeasts$? Or milk in general? Some concluded that it must mean the "Omnicorp MegaMilk" that had taken the world by storm over the last few years. Links from people who had drunk a large amount of this milk over the last few weeks had mysteriously disappeared, and at the scene of every disappearance this milk had been located. Not that it was much of a surprise due to its popularity. It would be similar to claiming TVs were causing it because they all had one. Either way no one knew and no one was giving any more information. And no one wanted to test out if the theory was correct or not.
	
	The Army arrived shortly after they issued the statement. They simply quarantined the City. Anyone trying to leave would be eliminated on sight as per marshal law rules.
	
	Things escalated from there. Riots sprouted all over the city. Either truly concerned citizens or opportunistic thugs. It did not matter. The police force was divided and subsequently crushed under the weight of the milky tide that grew in the confusion. The random scattered reports of these beasts becoming ever more common until they soon were unable to avoid. The riots soon stopped, as most had been converted into the $translation:info.bovine$ herds that now controlled the streets. Their numbers continued to grow as the human numbers decreased. It was rare to see a human at this point, instead you would see these roaming $translation:info.type$s fondling each other and suckling each other breasts.
	
	Suddenly electricity was cut off, phone lines, internet and soon after satellite internet as well. The army must be trying to hide what is happening and no doubt the people inside, spreading these terrifying images and videos were not helping with the armies quarantine.
	
	Homes were slowly broken into as the $translation:info.type$s searched for more humans. As if an instinct was ingrained into them to do so. Their ability to sense humans was downright unnatural. Luckily for you you went somewhere they could not easily get to. Your apartment complexes roof was only accessible via a ladder. A few $translation:info.type$s every so often tracked you down, but were unable to climb the ladder. Either not realizing they could, or just unable to handle it with their bodies the way they were. Either way you managed to avoid the initial pandemonium this way. But other problems are arising. You only have enough food and water to last a couple days. And you must enter the city, full of what the people coined before they overtook the city. As the "Changed"
	
	Now you must brave the "$translation:info.type$ Apocalypse"`,

	PROJECTS: {
		COMBAT: {
			COST: 2,
			EFFECT: `Gives +1 to all combat rolls per level.`,
			DESCRIPTION: `Utilizing your materials, you can make makeshift weapons designed to best the Cowgirls in one on one combat. Namely, just blunt hard hitting objects designed to stun as their resiliance to being hurt is high. But their composure is low and you only need an opening.`,
		},
		DEFENSE: {
			COST: 2,
			EFFECT: `Gives +1 to defense per level.`,
			DESCRIPTION: `The greatest danger in this time is from the Cowgirls themselves. Using some of the scrap material you have found, you are able to craft a combination of makeshift armour, designed entirely to soften blows and protect your mouth from unwanted milk.`,
		},
		UTILITY: {
			COST: 2,
			EFFECT: `Gives +1 to all Challenge rolls.`,
			DESCRIPTION: `Combat and defense are all well and good. But your problems are not just limited to these two categories. Strong shoes for running, lockpicks and even binoculars will be as, if not more useful than a slightly better mask.`,
		},
	},
	SKILLS: {
		STRENGTH: {
			UPGRADE: `Primarily used in combat and sometimes in challenges requiring brute force.`,
			CREATION: `Used primarily in combat and rarely in challenges.`,
		},
		INTELLIGENCE: {
			UPGRADE: `Primarily used in almost every challenge.`,
			CREATION: `Used primarily in challenges.`,
		},
		AGILITY: {
			UPGRADE: `Also used in some challenges.`,
			CREATION: `Used equally in combat and challenges.`,
		},
		STAMINA: {
			UPGRADE: `Increases your maximum health by 2 per point. Also used in some challenges.`,
			CREATION: `Used rarely in challenges. Increases health by 2 per point.`,
		},
		PERCEPTION: {
			UPGRADE: `Used in almost every challenge. Rarely used elsewhere.`,
			CREATION: `Used primarily in challenges.`,
		},
	},

	REWARDS: {
		FRIENDLYTEXT: {
			EXPERIENCE: 'Experience',
			FOOD: 'Food',
			MATERIALS: 'Materials',
			HEALTH: 'Health',
			PART_ANTENNA: 'a Working Antenna',
			PART_GENERATOR: 'a Functional Generator',
			PART_RADIO: 'a Ham Radio',
			OUTSKIRT: 'have opened up a new area',
			MOOER: 'a new location, "The Office"',
		},
	},

	ENDOFDAY: {
		INFECTION3: {
			GOOD: `Your mind once again braces itself against the milks induced thoughts and feelings. Your hands grope your breasts against your will as they cry out for attention. You tweak your teat, a squirt of milk escaping them. Your mind almost gives in as thoughts of milking, pleasure and joining your herd flood your mind. Somehow though, you manage to resist regaining your composure once more. But for how long?`,
			BAD: `Once again your bodies altered state tries to 'Correct' your mind. Your head fills with thoughts of milking, of your breasts being fondled by other $translation:info.type$s. Your nipples go erect as milk begins to leak from them. Your already constantly warm slit flares up as those thoughts bounce around your mind. And then it happens. You let out a soft moo. Your old life begins to be crushed by the new memories and feelings infiltrating your mind. Your body and mind has finally given in to the infectious milk.`,
		},

		EMPTYFOOD: `You have no food and more importantly no water. You can survive for now. But if you cannot find any tomorrow, you may have to resort to desperate messures. There is a lot of contaminated food and milk around...`,
		NOFOOD: `You cannot stomach another day without food. Especially with all the activity you have been doing recently. You only have one source left. Food made with milk. You tentativly eat it, hoping nothing happens. But soon your stomach begins to churn. Not in an unpleasant way which is even more worrysome.`,
	},

	// A way in
	AWAYIN: `Walking past one of the subway entrances, you notice the metal grate that had barred entry has been lifted slightly. No doubt when the power went down, the mechanism holding it became pliable to force. The handiwork looks to be of a person rather than one of the changed. With no sign of milk, it is probably a safe bet to guess it was human hands that did this. If so, this could provide a safe way into the inner city. Making sure to mark this on your map, you head inside. It takes a while but you manage to find a non blocked passageway into the City Centre. It will be more dangerous for sure, but it will also provide a much more varied bounty of supplies and goods. Today may be over, but tomorrow this could be the breakthrough you need to survive.`,
	// You gained X experience and have opened up a new area!

	INFECTION: {
		BASEDESCRIPTION: {
			MALE: `Your body's that of a normal human Male (For now). Toned hairy chest, strong arms and legs dripped with a good amount of muscle. Wearing whatever clothes you can scavenge you are still human. Between your thighs is an average sized cock showing you to be of the male and that is quite an accomplishment considering what is going on.`,
			FEMALE: `Your body is that of a normal human Female (For now). A pair of B cup breasts rest upon your chest, two smooth somewhat strong arms and legs are your current appendages. You are currently wearing whatever clothes you can scavenge that easily fit your human body that are not covered in milk or other fluids. Between your thighs is a labia leading to a vagina showing you to be of the Female persuasion, but that means you are not that far off the Changed that wander around the City. A terrifying thought`,
		},
		CONSTANT: {
			MALE: {
				DESCRIPTION: [
					`Your body is covered in $translation:a thin layer of white fur|smooth hairless human skin$ much like all the changed that roam the streets.Your chest has two tender and sore nipples that cry out in pleasure from the lightest touch.But you are not a $translation:info.cow$... yet. But your thoughts constantly drift to ideas of giving in and drinking more of that sweet milk, being a docile $translation:info.type$ could be fun right?`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`Your body is covered in $translation:a thin layer of white fur much like all the changed that roam the streets|smooth feminine skin, while it was smooth before, it now is almost completely hairless, any wrinkles and smudges have been removed and the lightest touch sends a shiver over your body$.Your chest has two tender and sore nipples that cry out in pleasure from the lightest touch.But you are not a $translation:info.cow$... yet.But your thoughts constantly drift to ideas of giving in and drinking more of that sweet milk, being a docile $translation:info.type$ could be fun right?`,
				],
			},
		},

		// infection
		MOOERNOUDDER: <IInfectionText>{
			MALE: {
				DESCRIPTION: [
					`$redirect.infection.constant.male.description.0$`,
					`Your entire body is covered in $translation:a thick layer of white fur|smooth blemishless pink feminine skin$ just like the changed that wander the streets. Your muscles are hidden by smooth looking feminine arms and legs, they are there but covered in a layer of fat to give you a perfect $translation:bovine|curvy$ shape. Your gender has been forcibly changed against your will, yet you find it feels so good as a need to fill your slit and vagina's emptiness grows stronger with every passing moment.`,
					`Your body is identical to the changed that wander around the city streets. Your entire body is covered in $translation:white fur|pink, smooth blemishless and oh so feminine skin$. Your body a slightly chubbier (But not at all fat) shape keeping a rather lean hourglass figure despite the chubbiness. Your chest has two large DD cup breasts hanging there, bouncing constantly from the slightest movement. Your sore teat like nipples constantly leaking a trail of milk.

					You constantly think about sex and milk, your breasts calling for you to be milked and your slit to be filled with anything, preferably someone just like yourself.`,
				],
				PROGRESS: [
					`You can feel your body begin to twist and churn. The infectious milk starting its transformative effects against your body. A tingle spreads over your skin as your skin begins to lose its hair. Blemishes vanish too as the tingle spreads. Your $translation:soft feminine skin grows coarse as a thin coating of white fur sprout to coat it in fuzz|skin grows soft and feminine$ just like those you see almost every day.

					Suddenly your chest expands out with a subtle but obvious pleasure as it grows tender and sore. The flesh around your nipples become sensitive to any touch be it on purpose or an accidental brushing against your clothing.

					Luckily it stops there. The effects of the milk seem to have passed away. But not without any effects.You best not drink any more milk. Who knows if the next amount will be enough to remove your humanity forever. Not that the idea of being a docile milk laden $translation:info.type$ is particularly unappealing...`,
					`You let it happen again! It does not take long before you're already changed body progresses through another transformation. $translation:Your skin tingles as your coat of white fur grows thicker, soon becoming a full coat of white bovine fur on top of soft feminine skin. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple feminine legs and arms. Your face screams out in pain as it begins to push outwards. A muzzle starts to slowly extend as the white fur covering your skin spreads up your neck and covers your head. Once it reaches your face it has a lovely bovine muzzle to coat as well|Your skin tingling as your smooth feminine skin softens even more so, soon removing every blemish and imperfection. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple feminine legs and arms. Your face screams out in pain as it begins to push outwards. The tingling reaches your face and while you cannot see it, you can feel your face soften, your lips fullen and your hair droop down your back.$. Your eyes improve in vision as they shift from human to $translation:info.bovine$ eyes and your ears double in sensitivity $translation:as they extend outwards in long leathery hide ears|alongside your eyes$.

					Next your cock grows hard, then as soon as it grows fully erect it begins to fade, a pulling sensation surrounding your hard member. Reaching down to grasp it you grab nothing but air, your dick vanishing before your very eyes. Your cock sinks into a warm fold sending a shudder of warm pleasure over your body. Your cock is gone replaced instead with a warm empty vagina that is begging for use. Having your gender forcibly changed before you should concern you, but it feels...right? Not to mention oh so pleasant. If only you could find someone to help you...`,
					`Either on purpose or by accident you have swallowed another sizeable amount of milk.

					Your body only has a few more physical changes left to make to convert it entirely into a Mooer's strong, sturdy and dumb body. Your thighs plump into thick voluptuous size. Hips growing wide into  broad birthing hips. Your empty Vagina flares up with even more lust. A dreadful feeling of emptiness washes over you. Begging you to get someone or something inside it. Be it from a $translation:info.type$ or from something else. 

					The sensitive flesh around your nipples starting to slowly (at first) push outwards quickly gaining in speed as a warm liquid begins to fill the insides of them.Your new breasts grow larger and larger filling with that warm liquid.Passing C, D before ending at two large jiggling and oh so sensitive DD cup breasts, leaking a trickle of milk from your nipples that have grown in size to look more like teats than breasts.

					The final change come to your body as your feet begin to feel stiff.Your flat long feet pull into your ankles as it forms a round stub. This stub soon is coated in a tough bone light substance as your feet merge into two hard bovine hooves.

					Your mind goes cloudy. Your head aches, but not with pain. No it's an ache of confusion and desire. Your body cries out to be used. Your breasts leaking with delicious creamy milk but no one to give it to. It demands to be touched, fondled and groped but no one to do so. Your head dulls as complex thoughts fade away to these base desires of yours. A desire to go visit the herd.`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`$redirect.infection.constant.female.description.0$`,
					`Your entire body is covered in $translation:a thick layer of white fur just like the changed that wander the streets|perfect blemishless, hairless pink skin$. Your muscles are hidden by smooth looking feminine arms and legs, they are there but covered in a layer of fat to give you a perfect $translation:bovine|feminine (but slightly stocky)$ shape.An overwhelming desire to fill your slit and vagina's grows stronger with every passing moment.`,
					`Your body is identical to the changed that wander around the city streets. Your entire body is $translation:covered in white fur|entirely covered in perfect, model-like pink skin$. Your body a slightly chubbier (But not at all fat) shape keeping a rather lean hourglass figure despite the chubbiness. Your chest has two large DD cup breasts hanging there, bouncing constantly from the slightest movement. Your sore teat like nipples constantly leaking a trail of milk.

					You constantly think about sex and milk, your breasts calling for you to be milked and your slit to be filled with anything, preferably someone just like yourself.`,
				],
				PROGRESS: [
					`You can feel your body begin to twist and churn. The infectious milk beginning to unleash its effects against your body. A tingle spreads over your skin as your skin begins to soften. Blemishes vanish into thin air as the tingle spreads. $translation:Your soft feminine skin grows coarse as a thin coating of white fur sprouts to coat it in fuzz just like those you see almost every day|Looking similar to those you see almost every day but not quite there yet, you can still see a few imperfections$.

					Your chest expands outward and with an overpowering pleasure your breasts grow ever so tender and sore. Your nipples grow hard as they poke out into your clothing the rub against them causing your knees to buckle as the pleasure washes over your body.

					Then suddenly it stops. The milks effects seem to have passed, but not without any effects.Your breasts continue to be extra sensitive and sore, nipples poking it's way through your clothing. You best not drink any more milk. Who knows if the next amount will be enough to remove your humanity forever. Not that the idea of being a docile milk laden $translation:info.type$ is particularly unappealing...`,
					`You let it happen again! It does not take long before your already changed body progresses through another transformation. $translation:Your skin tingles as your coat of white fur grows thicker, soon becoming a full coat of white bovine fur on top of soft feminine skin|Your skin tingling as your smooth feminine skin softens even more so, soon removing every blemish and imperfection$. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple $translation:fat on your|feminine$ legs and arms. Your face screams out in pain as it begins to push outwards. $translation:It extends slowly as the white fur covering your skin spreads up your neck and covers your head. Once it reaches your face it has a lovely bovine muzzle to coat as well|The tingling reaches your face and while you cannot see it, you can feel your face soften, your lips fullen and your hair droop down your back$. Your eyes improve in vision as they shift from human to bovine eyes and your ears double in sensitivity $translation:as they extend outwards in long leathery hide ears|alongside your eyes$.

					Next your vagina explodes into a series of pulses, racking your body with need the pulses sending your knees to buckle as they grow in intensity.You can barely contain your moaning before a long "Moooooooooo" of pleasure escapes your lips. Did you just… but it feels... right? And oh so pleasant. If only you could find someone to help you down there too...`,
					`Either on purpose or by accident you have swallowed another sizeable amount of milk.

					Your body only has a few more physical changes left to make to convert it entirely into a Mooer's strong, sturdy and dumb body. Your thighs plump into thick voluptuous size. Hips growing wide into  broad birthing hips. Your empty Vagina flares up with even more lust. A dreadful feeling of emptiness washes over you. Begging you to get someone or something inside it. Be it from a $translation:info.type$ or from something else. 

					The sensitive flesh around your nipples starting to slowly (at first) push outwards quickly gaining in speed as a warm liquid begins to fill the insides of them.Your new breasts grow larger and larger filling with that warm liquid.Passing C, D before ending at two large jiggling and oh so sensitive DD cup breasts, leaking a trickle of milk from your nipples that have grown in size to look more like teats than breasts.

					The final change come to your body as your feet begin to feel stiff.Your flat long feet pull into your ankles as it forms a round stub. This stub soon is coated in a tough bone light substance as your feet merge into two hard bovine hooves.

					Your mind goes cloudy. Your head aches, but not with pain. No it's an ache of confusion and desire. Your body cries out to be used. Your breasts leaking with delicious creamy milk but no one to give it to. It demands to be touched, fondled and groped but no one to do so. Your head dulls as complex thoughts fade away to these base desires of yours. A desire to go visit the herd.`,
				],
			},
			ENDING: 5,
		},
		// cowgirlInfection
		MOOERUDDER: <IInfectionText>{
			MALE: {
				DESCRIPTION: [
					`$redirect.infection.constant.male.description.0$`,
					`Your entire body is covered in $translation:a thick layer of white fur|smooth blemishless pink feminine skin$ just like the changed that wander the streets. Your muscles are hidden by smooth looking feminine arms and legs, they are there but covered in a layer of fat to give you a perfect $translation:bovine|curvy$ shape. Your gender has been forcibly changed against your will, yet you find it feels so good as a need to fill your slit and vagina's emptiness grows stronger with every passing moment.`,
					`Your body is identical to the changed that wander around the city streets.Your entire body is covered in $translation:white fur|pink, smooth blemishless and oh so feminine skin$. Your body a slightly chubbier (But not at all fat) shape keeping a rather lean hourglass figure despite the chubbiness. Your chest has two large DD cup breasts hanging there, bouncing constantly from the slightest movement.Your sore teat like nipples constantly leaking a trail of milk.$translation:In between your legs is a large, baggy, sensitive and constantly sloshing udder, full to the brim with milk. It's teats leaking a steady trial of milk at all times|$.

					You constantly think about sex and milk, your breasts calling for you to be milked and your slit to be filled with anything, preferably someone just like yourself.`,
				],
				PROGRESS: [
					`You can feel your body begin to twist and churn. The infectious milk beginning to unleash its effects against your body. A tingle spreads over your skin as your skin begins to lose it's hairs. Blemishes vanish too as the tingle spreads. Your $translation:soft feminine skin grows coarse as a thin coating of white fur sprout to coat it in fuzz|skin grows soft and feminine$ just like those you see almost every day.

					Suddenly your chest expands out with a subtle but obvious pleasure as it grows tender and sore. The flesh around your nipples become sensitive to any touch, be it on purpose or an accidental brushing from your clothing.

					Luckily it stops there. The milks effects seem to have passed away, but not without any effects.You best not drink any more milk.Who knows if the next amount will be enough to remove your humanity forever. Not that the idea of being a docile milk laden $translation:info.type$ is particularly unappealing...`,
					`You let it happen again! It does not take long before your already changed body progresses through yet another transformation. Your skin tingles as $translation:your coat of white fur grows thicker, soon becoming a full coat of white bovine fur on top of soft feminine skin. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple feminine legs and arms. Your face screams out in pain as it begins to push outwards. It extends slowly as the white fur covering your skin spreads up your neck and covers your head. Once the fur reaches your face it has a lovely bovine muzzle to coat as well|your smooth feminine skin softens even more so, soon removing every blemish and imperfection. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple feminine legs and arms. Your face screams out in pain as it begins to push outwards. The tingling reaches your face and while you cannot see it, you can feel your face soften, your lips fullen and your hair droop down your back$. Your eyes improve in vision as they shift from human to $translation:info.bovine$ eyes and your ears double in sensitivity $translation:as they extend outwards in long leathery hide ears|alongside your eyes$.

					Next your cock grows hard, then as soon as it grows hard it begins to fade. A pulling sensation surrounds your hard member. Reaching down to grasp it you grab nothing but air. Your cock sinks into a warm fold, sending a shudder of warm pleasure over your body. Your cock is gone replaced instead with a warm empty vagina that is begging for use. Having your gender forcibly changed before you should concern you. But it feels...right? Not to mention oh so pleasant. If only you could find someone to help you...`,
					`Either on purpose or by accident you have swallowed another sizeable amount of milk.

					Your body only has a few more physical changes left to make to convert it entirely into a Mooer's strong, sturdy and dumb body. Your thighs plump into thick voluptuous size. Hips growing wide into broad birthing hips. Your empty Vagina flares up with even more lust. A dreadful feeling of emptiness washes over you. Begging you to get someone or something inside it. Be it from a $translation:info.type$ or from something else. 

					The sensitive flesh around your nipples starting to slowly (at first) push outwards quickly gaining in speed as a warm liquid begins to fill the insides of them.Your new breasts grow larger and larger filling with that warm liquid.Passing C, D before ending at two large jiggling and oh so sensitive DD cup breasts, leaking a trickle of milk from your nipples that have grown in size to look more like teats than breasts.$translation:Just above your slit a bulge begins to form. A pink jiggling sensitiveness and warm sack forms filling quickly with a warm liquid just like your breasts. It inflates as it fills, four nubs appearing on the base. They extend outwards into 4 long pink teats, leaking with milk on top of a large jiggly sensitive udder overflowing with milk.

					|$ The final change come to your body as your feet begin to feel stiff.Your flat long feet pull into your ankles as it forms a round stub. This stub soon is coated in a tough bone light substance as your feet merge into two hard bovine hooves.

					Your mind goes cloudy. Your head aches, but not with pain. No it's an ache of confusion and desire. Your body cries out to be used. Your breasts leaking with delicious creamy milk but no one to give it to. It demands to be touched, fondled and groped but no one to do so. Your head dulls as complex thoughts fade away to these base desires of yours. A desire to go visit the herd.`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`$redirect.infection.constant.female.description.0$`,
					`Your entire body is covered in a $translation:thick layer of white fur just like the changed that wander the streets|perfect blemishless, hairless pink skin$. Your muscles are hidden by smooth looking feminine arms and legs, they are there but covered in a layer of fat to give you a perfect $translation:bovine|feminime (but slightly stocky)$ shape.An overwhelming desire to fill your slit and vagina's grows stronger with every passing moment.`,
					`Your body is identical to the changed that wander around the city streets. Your entire body is covered in $translation:white fur|pefect, model-link pink skin$. Your body a slightly chubbier (But not at all fat) shape keeping a rather lean hourglass figure despite the chubbiness. Your chest has two large DD cup breasts hanging there, bouncing constantly from the slightest movement. Your sore teat like nipples constantly leaking a trail of milk.

					You constantly think about sex and milk, your breasts calling for you to be milked and your slit to be filled with anything, preferably someone just like yourself.`,
				],
				PROGRESS: [
					`You can feel your body begin to twist and churn. The infectious milk beginning its effects against your body. A tingle spreads over your skin as your skin begins to soften. Blemishes vanishing into thin air  as the tingle spreads. $translation:Your soft feminine skin grows coarse as a thin coating of white fur sprouts to coat it in fuzz just like those you see almost every day|Looking similar to those you see almost every day but not quite there yet, you can still see a few imperfections$.

					Your chest moans out with an overpowering pleasure your breasts growing ever so tender and sore. Your nipples grow hard as they poke out into your clothing, rubbing against them causing your knees to buckle as pleasure washes over your body.

					Then suddenly it stops. The milks effects seem to have passed away. But not without any effects.Your breasts continue to be extra sensitive and sore. Your nipples poke their way through your clothing, signalling that you best not drink any more milk. Who knows if the next amount will be enough to remove your humanity forever. Not that the idea of being a docile milk laden cowgirl is particularly unappealing...`,
					`You let it happen again! It does not take long before your already changed body progresses through yet another transformation. Your skin tingles as your $translation:coat of white fur grows thicker, soon becoming a full coat of white bovine fur on top of soft feminine skin|smooth feminine skin softens even more so, soon removing every blemish and imperfection$. Your arms and legs slim down, removing any excess muscle, replacing it instead with soft supple $translation:fat on your|feminine$ legs and arms. Your face screams out in pain as it begins to push outwards. $translation:It extends slowly as the white fur covering your skin spreads up your neck and covers your head, Once it reaches your face it has a lovely bovine muzzle to coat as well|The tingling reaches your face and while you cannot see it, you can feel your face soften, your lips fullen and your hair droop down your back$. Your eyes improve in vision as they shift from human to $translation:info.bovine$ eyes and your ears double in sensitivity $translation:as they extend outwards in long leathery hide ears|alongside your eyes$.

					Next your vagina explodes into a series of pulses, racking your body with need, the pulses sending your knees to buckle as they grow in intensity. You can barely contain your moaning before a long "Moooooooooo" of pleasure escapes your lips. Did you just...but it feels... right? And oh so pleasant too. If only you could find someone to help you down there too...`,
					`Either on purpose or by accident you have swallowed another sizeable amount of milk.

					Your body only has a few more physical changes left to make to convert it entirely into a Mooer's strong, sturdy and dumb body. Your thighs plump into thick voluptuous size. Hips growing wide into  broad birthing hips. Your empty Vagina flares up with even more lust. A dreadful feeling of emptiness washes over you. Begging you to get someone or something inside it. Be it from a $translation:info.type$ or from something else. 

					The sensitive flesh around your nipples starting to slowly (at first) push outwards quickly gaining in speed as a warm liquid begins to fill the insides of them.Your new breasts grow larger and larger filling with that warm liquid.Passing C, D before ending at two large jiggling and oh so sensitive DD cup breasts, leaking a trickle of milk from your nipples that have grown in size to look more like teats than breasts$translation:. Just above your slit a bulge begins to form. A pink jiggling sensitiveness and warm sack forms filling quickly with a warm liquid just like your breasts. It inflates as it fills, four nubs appearing on the base. They extend outwards into 4 long pink teats, leaking with milk on top of a large jiggly sensitive udder overflowing with milk|$.

					The final change come to your body as your feet begin to feel stiff.Your flat long feet pull into your ankles as it forms a round stub. This stub soon is coated in a tough bone light substance as your feet merge into two hard bovine hooves.

					Your mind goes cloudy. Your head aches, but not with pain. No it's an ache of confusion and desire. Your body cries out to be used. Your breasts leaking with delicious creamy milk but no one to give it to. It demands to be touched, fondled and groped but no one to do so. Your head dulls as complex thoughts fade away to these base desires of yours. A desire to go visit the herd.`,
				],
			},
			ENDING: 5,
		},
		// cowgirlInfection2
		MILKER: <IInfectionText>{
			MALE: {
				DESCRIPTION: [
					`Your body has been infected by the transformative milk, your chest is home to two small (At least considering all the others) B cup breasts, constantly leaking milk from your perky nipples. Other than those two sensitive warm lumps, your body has smoothed out, but that is it. You are constantly thinking about being milked however...`,
					`Two DD cup breasts rest against your chest. Your body, covered in a $translation:thin coating of fur fails to hide|smooth and blemishless skin helps to show off$ your supple feminine curves, thick fat in all the right places, wide hips and jiggly rear. Your mind constantly drifts off to thoughts of being milked and suckled. Despite your effort to not think about it. Between your legs is a small cock, but for how long can you still say you are male?`,
					`Your body is that of a typical Milker. Large DD cup jiggly breasts constantly leak milk, bouncing to and fro with each step you make. Your curvy body is made not for strength, but for transporting your milk around, Between your legs is a dripping slit, ready for uses for anyone who can actually use it. All you desire though is to be milked and suckled, a good cow to be used as a milk machine, whenever and however they want.`,
				],
				PROGRESS: [
					`As the milkers milk flows through your stomach you immediately feel your stomach churn and complain. Your chest erupts into a pleasurable feeling, your nipples poke out against your shirt as your areola darken and gently your chest pushes outwards. You can feel the sensitive flesh coalesce as two breasts form on your body. They continue to grow, leaving you with two full feeling, warm and incredibly sensitive bouncy breasts. As you remove your top to see what has happened you notice a damp spot around your nipples. Your examination finds that you have milk leaking from your breasts in a constant and steady stream. As you touch your breasts they squirt, milk flowing out and against the floor from even the slightest prod or cup, and it feels so good! You need more! 

					You play with your breasts and milk yourself into a stupor.You awake hours later in a puddle of creamy sweet smelling milk.Your milk!$translation:Your skin, now silky smooth, begins to cover in a coating of thin hairs, half white, half black|You touch your skin, now silky smooth, completely hairless and soft to the touch$.Other than that (And your two B cup breasts) you do not look much different.But with any movement your breasts give a reminder of just how good it felt to play with them as they leak a little warm milk.`,
					`The changes are quick to come after you engulf more of the Changed's milk. You feel your breasts continue to tug on your sanity. The pleasure coming from them, as they leak their trail of milk, is orgasmic. And sure enough with more of the milk they proceed to grow. You can feel every inch of your chest expand, filling with even more milk and somehow, becoming even more sensitive feeling as they balloon out into two DD cup breasts. As you grope your body uncontrollably, milk rushes forth from your nipples, doubling the amount (And pleasure) from what you were capable of but a few moments ago. Your chest weighs heavily on your body, you just barely are able to tear your eyes away from your lovely jiggly chest to see the rest of your body change, not that you can remove your hands from them. Your $translation:fur covered skin sprouts out into a full coating of cowgirl looking fur. Your hands and feet begin to feel stiffer, inspecting them you notice a few of your fingers have merged into three pronged hard almost hoove like fingers, Your feet now complete bovine hooves|blemishless smooth skin somehow grows softer and more feminine$. Muscle melts away, replaced with a luscious appealing layer of fat. You can both see and feel your hips widen, your stomach tuck in and your thighs thicken. 

					Atop your smooth crotch a pink bulge begins to form.A rushing sensation comes to fill the sack's contents and it quickly dawns on you what it is.It's an udder just like a cow, four nubs pushing out from it's thin pink skin, lengthening out into two inch long teats.You can feel it fill with a warm liquid which you somehow know immediately is milk.Milk to be drank by your fellow Mooers and changed.Even the thought of it sends both your breasts and teats to leak a trickle of milk along with a content bliss over your body.

					Your mind begins to (Against your will) picture your breasts be cupped and played with.Your nipples suckled by… anyone who is willing to!These thoughts make your knees quiver and your body rise in heat.You can suppress them for now.But they won't stop creeping up from within.`,
					`Despite taking in more of the changed's milk your body has little to actually change at this point. Your face $translation:pushes out gently into a bovine muzzle, nose melting into the front. Ears move up the side of your head as they grow in length to form two long hide ears just like a cows.Two stubby horns poke through your scalp and appear on the top of your head. You can't imagine what more is left to change|softens into a overtly feminine appearance, lips growing fuller, hair turning a platinum blonde, two stubby horns make their way through your scalp and poke out from under your hair. muscle melts away to be replaced with luscious layers of fat. You think that this isn't too bad$, that is until you feel your one remaining male trait begins to slip away. Your cock begins to recede into your body with an orgasmic spurt of pleasure. You watch as the last remnants of your manhood slip away into warm dripping folds as your slit and vagina form to fully convert you into one of the changed.

					Your udder begins to fill with even more milk, it expanding by consequence as it tries to handle the influx of milk.Your teats are leaking a steady stream of milk as they too grow an extra couple of inches.Your conspicuous udder now taking up the majority of your crotch, bouncing, jiggling and sloshing with milk from even the slightest movement, and it feels so good when it does.

					But you are not completely there yet.You can feel it tug at your mind.A desire to simply let your body be suckled and played with by your fellow changed.You can hear they call out for you, to welcome you into the herd, to embrace you.You take a step towards the siren call before you remember yourself, you are not those things!Even if you look (And mostly act) like one… right!?`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`Your body has been infected by the transformative milk, your chest is home to two small (At least considering all the others) B cup breasts, constantly leaking milk from your constantly erect and perky nipples. Other than those two sensitive warm lumps, your body has smoothed out, but that is it. You are constantly thinking about being milked however...`,
					`Two DD cup breasts rest against your chest. Your body, covered in $translation:a thin coating of fur, doing nothing to hide|smooth blemishless skin, helping to show off$ your supple feminine curves, thick fat in all the right places, wide hips and jiggly rear. Your mind constantly drifts off to thoughts of being milked and suckled. Despite your effort to not think about it. Between your legs is a warm dripping slit, constantly reminding you of just how good your body feels like this. Don't you want to be milked?`,
					`Your body is that of a typical Milker. Large DD cup jiggly breasts constantly leak milk, bouncing to and fro with each step you make. Your curvy body is made not for strength, but for transporting your milk around, Between your legs is a dripping slit, ready for uses for anyone who can actually use it. All you desire though is to be milked and suckled, a good cow to be used as a milk machine, whenever and however they want. `,
				],
				PROGRESS: [
					`As the milkers milk flows through your stomach you immediately feel your stomach churn and complain. Your chest erupts into a pleasurable feeling, $translation:your nipples poking out against your shirt as your areola darken, and gently your chest starts to push outwards. You can feel the sensitive flesh coalesce as two breasts form on your body. They continue to expand, leaving you with two full feeling, warm and incredibly sensitive bouncy breasts|you can feel the nipples on your chest go fully erect and press against whatever clothing you have draped on top of them. You can feel the sensitive flesh coalesce as they begin to fill with something warm$. As you remove your top to see what has happened you notice a damp spot around your nipples. Your milk is leaking, producing a constant and steady stream from your nipples. As you touch your breasts they squirt, milk flowing out and against the floor from even the slightest prod or cup. And it feels so good! You need more! 

					You play with your breasts and milk yourself into a stupor.You awake hours later in a puddle of creamy sweet smelling milk.Your milk.Your skin, now silky smooth, $translation:begins to cover in a coating of thin hairs, half white, half black.Other than that (And your two B cup breasts)|completely hairless and soft to the touch.Other than that$ you do not look much different.But with any movement your breasts give a reminder of just how good it felt to play with them with a waft of sweet smelling milk soaking your top.`,
					`The changes are quick to come after you engulf more of the Changeds milk. You feel your breasts continue to tug on your sanity. The pleasure coming from them, as they leak their trail of milk, is orgasmic. And sure enough with more of the milk they proceed to grow. You can feel every inch of your chest expand, filling with even more milk and somehow becoming increasingly sensitive feeling as they balloon out into two easily DD cup breasts. As you grope your body uncontrollably, milk rushes forth from your nipples. It doubles the amount (And pleasure) from what you were capable of but a few moments ago. Your chest weighs heavily on your body, you just barely are able to tear your eyes away from your lovely jiggly chest to see the rest of your body change, not that you can remove your hands from them. Your $translation:fur covered skin sprouts out into a full coating of cowgirl looking fur. Your hands and feet begin to feel stiffer, inspecting them you notice a few of your fingers have merged into three pronged hard almost hoove like fingers, Your feet completely reform into bovine hooves|blemishless smooth skin somehow grows softer and more feminine$. Muscle melts away, replaced with a luscious appealing layer of fat. You can both see and feel your hips widen, your stomach tuck in and your thighs thicken. 

					Atop your smooth crotch a pink bulge begins to form.A rushing sensation comes to fill the sack's contents and it quickly dawns on you what it is.It is an udder just like a cow., four nubs push out from it's thin pink skin, lengthening out into two inch long teat.You can feel it fill with a warm liquid which you somehow know immediately is milk.Milk to be drank by your fellow Mooers and changed.Even the thought of it sends both your breasts and teats to leak a trickle of milk along with a content bliss over your body.

					Your mind begins to (Against your will) picture your breasts be cupped and played with.Your nipples suckled by… anyone who is willing to!These thoughts make your knees quiver and your body rise in heat.You can suppress them for now.But they won't stop creeping up from within.`,
					`Despite taking in more of the Changed's milk your body has little to actually change at this point. Your face $translation:pushes out gently into a bovine muzzle, nose melting into the front. Ears move up the side of your head as they grow in length to form two long hide ears just like a cows.Two stubby horns poke through your scalp and appear on the top of your head|softens into a overtly feminine appearance, lips growing fuller, hair turning a platinum blonde, two stubby horns making their way through your scalp and poking out of your hair. muscle melts away to be replaced with luscious layers of fat$. Your slit flares up with heat, as if trying to tell you something important. Your hand moves by reflex down towards it, begging you to caress and explore it. You just barely manage to resist it's call but it's need lingers in your mind.

					Your udder begins to fill with even more milk, it expanding by consequence as it tries to handle all the influx of milk.Your teats continue to leak a steady stream of milk as they too grow an extra couple of inches.Your $translation:inconspicuous|conspicuous$ udder now taking up the majority of your crotch, bouncing, jiggling and sloshing with milk from even the slightest movement, and it feels so good when it does.

					But you are not completely there yet.You can feel it tug at your mind.A desire to simply let your body be suckled and played with by your fellow changed.You can hear them call out for you, to welcome you into the herd, to embrace you.You take a step towards the siren call before you remember yourself, you are not those things!Even if you look (And mostly act) like one… right!?`,
				],
			},
			ENDING: 12,
		},
		// cowgirlInfection3
		HORNCOW: <IInfectionText>{
			MALE: {
				DESCRIPTION: [
					`Your body after becoming infected is constantly warm$translation:| despite losing all of its body hair$. Your chest home to two small A cup breasts, with darkened areola and small nipples. Your cock constantly is erect and despite your best efforts you cannot calm your body down. `,
					`Your chest is now covered in two large C cup breasts, with darkened areola and pert nipples, occasionally leaking a little bit of milk if you push them in the wrong way. Your figure is that of a rather exaggerated hourglass, wide hips, perky rear and elegant curves have consumed your body all with $translation:thin white and black fur with a small bovine muzzle|smooth blemishless skin$. Your cock constantly is still there, constantly erect and ready for use. You can feel the changes tug against your sanity, constantly bringing your thoughts to anyone playing with your breasts, or thrusting something inside you. With a genital you don't have. Yet.`,
					`Your body is now that of a HornCow. Thin hourglass figure$translation: covered in a coat of white and black fur|$, body laced with fat to give you sensual feminine curves, two DD cup breasts that constantly jiggle with each step you make. They produce milk ike all the others, but your nipples are nowhere near as large. Your breasts clearly meant for looking at and not purely for milking. Between your thighs is a constantly horny and dripping slit, begging for use at all times.Your mind drifts into fantasies whenever you are not completely focusing on something, picturing you getting fucked, having your breasts played with, or playing with whatever you can get your hands on.`,
				],
				PROGRESS: [
					`As the strange sensual change's milk settles in your stomach you are overcome by a warm and intoxicating pleasure, your body crying out in bliss as you squirm on the spot. Your cock hardens instantly as your thoughts sink into depravity, you think back to the mooer you just fucked, her body bouncing against your cock, her breasts jiggling with each thrust you made, you are lost in thought as your body begins to change, your skin softening, hair melting away leaving smoother more feminine skin behind$translation:, it's smoothness is soon interupted however by the coating of small hairs, black and white smatterings covering your body, much like those of the changed that roam the city|$, your cock shrinks a few inches in your hand as you grasp down upon it to try and relieve some of the pleasure. Two warm sensitive mounds push out from around your nipples, areola darkening as tiny nipples poke out as two a cup breasts form where none were before. The pleasure soon fades and your thoughts return to normal. You look over yourself and gasp, in your bliss you had not noticed the milks effects begin to work. You try to steel your mind, but your thoughts often slip back to the changed… who you wish to fuck once again.`,
					`Your mind once again sinks back to your time with that lovely changed. Her bouncing curvy body fills your vision as the overwhelming sense of pleasure returns. Your cock once more hardening into a fully erect state as you picture yourself fucking her once more. But this time it's a little different, you spend some time groping her breasts, suckling her teat and fingering her slit. And that feels even more arousing and pleasurable than the idea of inserting your cock within her.

					The transformative milk flows through your body, getting to work on correcting your non - changed bits.Your breasts fill with sensitive and aching flesh, your hands moving over to fondle them as they swell in your grasp gaining several cup sizes in mere moments. $translation:The thin coating of fur across your body begins to thicken into a full coat of white and black fur.Your masculine face softens before slowly creeping outwards into a soft bovine face, ears moving up the side of your head and growing into long but thin hide ears.And your overall body shape thins, muscle replaced with fat as your sides tuck in. 

					You feel your feet slowly cover in a rough hard substance, pushing your toes and the tip of your foot towards your heel, your feet melt into your heel before coating in a rough catriledge like material, you can no longer feel any toes, instead your feet are now two hooves, stomping about, yet somehow you feel as agile even with two feet that should be clumsier.|Whatever remaining hair on your skin fades away, giving you soft, smooth and perfect blemishless skin.Your masculine face softens, lips fill a little to give you a soft feminine and seductive face.And your overall body shape thins, muscle replaced with fat as your sides tuck in.$

					The changes stop and your mind returns to a 'normal' state.But you cannot stop thinking about fingering something, and there are plenty of mooers around of which you could grope, knead, suck and finger, if you just give in.`,
					`The infection flowing through you strengthens, your body heating up, welling with an intense pleasurable sensation that overwhelms you. Your short cock hardens but a pulling sensation soon engulfs it, a tug pulls it inside, warm sensitive folds form around the base as it is sucked inside with a burst of even stronger pleasure. Your new slit deepens, a vagina forming just beyond it leading to a vacant needy womb.

					Your hips widen, ass filling with jiggly fat as they become round and bubbly, your already large breasts swell with warm supple flesh into two DD cup breasts, nipples poking out of them in an obvious fashion.Your sides tuck in a little more, giving you a voluptuous hourglass figure and your thighs grow thick with fat.The last change comes to your body as your arms slim, removing all muscle leaving only curvy feminine arms behind, your hair flowing to your lower back as it turns a platinum blonde.

					Your mind stays in a constant horny state, despite what resilience you try to put up, you cannot stop thinking about sex, fondling Changed or anything you can get your hands on.Even thinking about such things makes your slit leak, your crotch throb with pleasure and your mouth moan.You can control yourself, just barely.But with the wrong look at a changed. Your mind may change, forever.`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`Your body after becoming infected is constantly warm. Your chest home to two pleasure filled breasts, with darkened areola and constantly erect nipples. Your nipples constantly flood you with please and despite your best efforts you cannot calm your body down. `,
					`Your chest is now covered in two large C cup breasts, with darkened areola and pert nipples, occasionally leaking a little bit of milk if you push them in the wrong way. Your figure is that of a rather exaggerated hourglass, wide hips, perky rear and elegant curves have consumed your body all with $translation:thin white and black fur with a small bovine muzzle|smooth blemishless skin$. You can feel the changes tug against your sanity, constantly bringing your thoughts to anyone playing with your breasts, or thrusting something inside you.`,
					`Your body is now that of a HornCow. Thin hourglass figure covered in $translation:a coat of white and black fur|soft skin$, body laced with fat to give you sensual feminine curves, two DD cup breasts that constantly jiggle with each step you make. They produce milk ike all the others, but your nipples are nowhere near as large. Your breasts clearly meant for looking at and not purely for milking. Between your thighs is a constantly horny and dripping slit, begging for use at all times.Your mind drifts into fantasies whenever you are not completely focusing on something, picturing you getting fucked, having your breasts played with, or playing with whatever you can get your hands on.`,
				],
				PROGRESS: [
					`As the strange sensual Changes milk settles in your stomach you are overcome by a warm and intoxicating pleasure, your body cries out in bliss as you squirm on the spot. Your slit pulses with pleasure your thoughts sink into depravity, you think back to the mooer you just engaged with sexually, her breasts jiggling as she fingered you, the warmth against your breasts as she pressed them into yours, the intoxicating scent of milk surrounding you as it leaked all over you. You are lost in thought as your body begins to change, your skin softening, hair melting away leaving smoother even more feminine skin behind, it's smoothness is soon interupted however by the coating of small hairs, black and white smatterings covering your body, much like those of the changed that roam the city. Your breasts begin to feel evermore sensitive as they grow within your groping hands, trying desperately to relieve some of the pleasure circling your body areola darkening as your nipples grow erect, permanently. The pleasure soon fades and your thoughts return to normal. You look over yourself and gasp, in your bliss you had not noticed the milks effects begin to work. You try to steel your mind, but your thoughts often slip back to the Changed… who you wish to play with once again.`,
					`Your mind once again sinks back to your time with that lovely changed. Her bouncing curvy body fills your vision as the overwhelming sense of pleasure returns but this time it's a little different, you spend some time groping her breasts, suckling her teat and fingering her slit. And that feels even more arousing and pleasurable than before.

					The transformative milk flows through your body, getting to work on correcting your non - changed bits.Your breasts fill with more sensitive and aching flesh, your hands moving over to fondle them as they swell in your grasp, gaining several cup sizes in mere moments.$translation:The thin coating of fur across your body begins to thicken into a full coat of white and black fur.Your face softens before slowly creeping outwards into a soft bovine face, ears moving up the side of your head and growing into long but thin hide ears.And your overall body shape thins, muscle replaced with fat as your sides tuck in. 

					You feel your feet slowly cover in a rough hard substance, pushing your toes and the tip of your foot towards your heel, your feet melt into your heel before coating in a rough catriledge like material, you can no longer feel any toes, instead your feet are now two hooves, stomping about, yet somehow you feel as agile even with two feet that should be clumsier.|Whatever remaining hair on your skin fades away, giving you soft, smooth and perfect blemishless skin.Your face softens, lips fill a little to give you an even more softer feminine and seductive face.And your overall body shape thins, muscle replaced with fat as your sides tuck in.$

					The changes stop and your mind returns to a 'normal' state.But you cannot stop thinking about fingering something, and there are plenty of mooers around of which you could grope, knead, suck and finger, if you just gave in.`,
					`The infection flowing through you strengthens, your body heating up, welling with an intense pleasurable sensation that overwhelms you. Your slit cries out in a mixture of need and emptiness, images of Changed filling you with whatever they have at their disposal floods your mind and the heat threatens to consume you entirely.

					Your hips widen, ass filling with jiggly fat as they become round and bubbly, your already large breasts swell with warm supple flesh into two DD cup breasts, nipples poking out of them in an obvious fashion.Your sides tuck in a little more, giving you a voluptuous hourglass figure and your thighs grow thick with fat.The last change comes to your body as your arms slim, removing all muscle leaving only curvy feminine arms behind, your hair flowing to your lower back as it turns a platinum blonde.

					Your mind stays in a constant horny state, despite what resilience you try to put up, you cannot stop thinking about sex, fondling Changed or anything you can get your hands on.Even thinking about such things makes your slit leak, your crotch throb with pleasure and your mouth moan.You can control yourself, just barely.But with the wrong look at a changed, your mind may change, forever.`,
				],
			},
			ENDING: 12,
			HUCOWENDING: 13,
		},
		// cowgirlInfection4
		HERMCOW: <IInfectionText>{
			MALE: {
				DESCRIPTION: [
					`Your body is slightly more muscular than you remember, your skin however is smooth and feminine$translation: with small black and white hairs covering your skin|$. On your chest are two A cup breasts, the telltale sign of being infected by whatever it is that turns people into Changed. Your cock is permanently erect, making your walks a little wobbly from your hard member. It doesn't matter what you think, do some meditation or eat food. Your cock is ALWAYS erect, ready and willing for use. `,
					`Your body is a mixture of Male and female parts. Skin smooth and decisively feminine, on your chest is two large jiggly C cup breasts. Your overall body shape is more muscular and your cock is larger than it was before you got infected. Your larger cock feels really good, constantly hitting you with pleasure, keeping you in a constantly horny state.`,
					`Your body is now that off a HermCow. A rather rare species o Changed you have only ever seen one of before. Your chest has 2 milky C cup breasts, body muscular more than curvy. Between your thick thighs is a 10 inch, fully erect cock, leaks some pre as it readies itself for use at all times.`,
				],
				PROGRESS: [
					`The milk from that... hermaphrodite? Changed courses through you, you immediately feel a warmth engulf your cock, it hardens as your body pulses with pleasure, your mind focuses on your cock, failing to notice $translation:hair sprouting all over your body. Thickening into a (albeit it thin) coat of fur, white and black just like the changed that wander the streets|the hairs evaporating from your body$. Areola darkening as sensitive flesh begins to coalesce around your nipples, gently pushing your chest outwards into two A cup breasts. Then the changes stop, but the pleasure surrounding your cock remains, pulsing with pleasure, each pulse stronger than the last, your mind slowly sinks back into your control, but when you come to your senses your cock remains hardened, occasionally pulsing with pleasure once again, as if telling you it is ready for use and with all the changed around, it would be easy to find one, after you have had a fill of milk first.`,
					`The changes to your body come once again, your breasts ringing out in a pleasurable feeling, a filling sensation both internally and externally signal the start of your milk production, your nipple poking out as they become fully erect, a trickle of milk escaping your warm $translation:breasts, soaking into your now full coat of bovine fur covering your voluptuous|breasts and titillating the skin as it rolls down your engorged, now$ C cup breasts. Meanwhile the rest of your body begins to smooth more,$translation:| any remaining hair fading from your body as$ muscle becoming hidden behind thin delicate layers of scintillating and appealing fat, changing your body into smooth curves. Your constantly erect cock also begins to tug, pulling outwards you can see it grow several inches in length, barely able to be hidden under any clothing and constantly erect and sensitive. 

					Your thoughts are infiltrated by your hormones, images of your breasts being played with fill your mind, after a suitable amounts of fondling they leap to your cock, engulfing it in their mouth, between their breasts or even deep inside their body and you love every minute of it.$translation:

					You feel your feet slowly cover in a rough hard substance, pushing your toes and the tip of your foot towards your heel, your feet melt into your heel before coating in a rough catriledge like material, you can no longer feel any toes, instead your feet are now two hooves, stomping about, yet somehow you feel as agile even with two feet that should be clumsier.|$

					You snap back to reality as your cock dribbles some pre, its desire to be used resonating through your mind, and any thought about the Changed immediately turns to ones of pleasure and fucking.`,
					`Your cock flares up with the heat that has been plaguing your body for days, your hardened cock begins to swell as it grows yet another couple of inches. Dripping a little with cum as it gives in to it's overwhelming need and pleasure. 

					Your overall body does not change much$translation:, but your head changes a lot.Your| other than your$ hair lengthening down your back into a stark red.$translation:Your masculine face softens before slowly creeping outwards into a soft bovine face, ears moving up the side of your head and growing into long but thin hide ears|Your face softening as lips plumpen$.Your sides tuck in just a little as your hips widen and ass plumpen into two jiggly mounds.

					But then, just below your cock, a new overwhelming sensation comes.You can feel something new and sensitive be created, an opening forming just low your hardened erect member.You tear off anything blocking its way, to find warm, wet dripping folds now taken refuge atop a small mound between your thighs.As if it waited for you to know it was there an intense emptiness formed as your vagina and womb were created within you, new hormones rushed through your body and thought with your masculine hormones flooding from your hard member.Then a wave of peace washed over you.Your needy cock and needy slit formed a bond of unison, both taking equal amounts of desire, both equal amounts of need in your thoughts.

					Then your mind turned to the Changed.Those needy but useful changed, wandering around without a care and ready for someone to take 'Care' of them and be filled with your lovely cock.Even if they don't know they need it yet.

					No wait!You're a human!Not… A Hermaphrodite Changed? Right? Your cock and slit would say otherwise.`,
				],
			},
			FEMALE: {
				DESCRIPTION: [
					`Your body is slightly more muscular than you remember, your skin however is smooth and feminine. On your chest are two breasts filling with something which you assume is milk, the telltale sign of being infected by whatever it is that turns people into Changed. Your body feels really warm, specifically on the front of your crotch, a newfound warmth which you cannot quite pin down. This warmth makes your walks a little wobbly from your. It doesn't matter what you think, do meditation or eat food. That spot remains warm and distracting. `,
					`Your body is a mixture of Male and female parts. Skin smooth and decisively feminine, on your chest is two large jiggly C cup breasts. Your overall body shape is more muscular and your cock is larger than it was before you got infected. Your larger cock feels really good, constantly hitting you with pleasure, keeping you in a constantly horny state.`,
					`Your body is now that off a HermCow. A rather rare species o Changed you have only ever seen one of before. Your chest has 2 milky C cup breasts, body muscular more than curvy. Between your thick thighs is a 10 inch, fully erect cock, leaks some pre as it readies itself for use at all times.`,
				],
				PROGRESS: [
					`The milk from that... hermaphrodite? Changed courses through you, you immediately feel a warmth engulf your slit, it flares up with need as your body pulses with pleasure, your mind focuses on $translation:your cock, failing to notice hair sprouting all over your body. Thickening into a (albeit it thin) coat of fur, white and black just like the changed that wander the streets. Areola|warm empty neediness, failing to notice the hairs evaporating from your body, your areola$ darkening even more as your nipples harden, gently your chest pushes outwards, breasts filling with an increased amount of sensitive flesh, growing an extra cup size in the process. Then the changes stop, but the pleasure surrounding your slit remains, pulsing with pleasure, each pulse stronger than the last, your mind slowly sinks back into your control, but when you come to your senses your slit remains moist and dripping, occasionally pulsing with pleasure once again, as if telling you it is ready for use and with all the changed around, it would be easy to find one, after you have had a fill of milk first.`,
					`The changes to your body come once again, your breasts ringing out in a pleasurable feeling, a filling sensation both internally and externally signal the start of your$translation:| milk$ production, your nipple poking out as they become fully erect, a trickle of milk escaping your warm $translation:breasts, soaking into your now full coat of bovine fur covering your voluptuous|breasts and titillating the skin as it rolls down your engorged, now$ C cup breasts. Meanwhile the rest of your body begins to smooth more, any remaining hair fading from your body as muscle too becomes hidden by layers of scintilating and volotptious fat, changing your body into smooth curves. Above your dripping slit a small and overly sensitive nub begins to push out from the empty skin, the sensitive nub rushes out from your crotch, an obvious cock tip now growing out of your crotch, just like a male's cock. It quickly reaches 6 inches long, erect and begging for use.

					Your thoughts are infiltrated by your hormones both new and old, images of your breasts being played with fill your mind, after a suitable amounts of fondling they leap to your cock, engulfing it in their mouth, between their breasts or even deep inside their body and you love every minute of it.$translation:|

					You feel your feet slowly cover in a rough hard substance, pushing your toes and the tip of your foot towards your heel, your feet melt into your heel before coating in a rough catriledge like material, you can no longer feel any toes, instead your feet are now two hooves, stomping about, yet somehow you feel as agile even with two feet that should be clumsier.$

					You snap back to reality as your cock dribbles some pre, its desire to be used resonating through your mind, and any thought about the Changed immediately turns to ones of pleasure and fucking.`,
					`Your cock and slit flares up with the heat that has been plaguing your body for days, your hardened cock begins to swell becoming fully erect. Dripping a little with cum as it gives in to it's overwhelming need and pleasure. 

					Your overall body does not change $translation:much other than your hair|much, but your head changes a lot.Hair$ lengthening down your back into a stark red.Your face $translation:softening as lips plumpen|softens before slowly creeping outwards into a soft bovine face, ears moving up the side of your head and growing into long but thin hide ears$.Your sides tuck in just a little as your hips widen and ass plumpen into two jiggly mounds.

					Then, your new cock grows several inches longer, a rush of new hormones coursing through your body and thoughts with your masculine hormones flooding from your hard member.Then a wave of peace washed over you.Your needy cock and needy slit formed a bond of unison, both taking equal amounts of desire, both equal amounts of need in your thoughts.

					Your mind turns to the Changed.Those needy but useful changed, wandering around without a care and ready for someone to take 'Care' of them and be filled with your lovely cock.Even if they don't know they need it yet.

					No wait!You're a human!Not… A Hermaphrodite Changed? Right? Your cock and slit would say otherwise.`,
				],
			},
			ENDING: 14,
		},
	},

	COWGIRL: {
		MOOERNOUDDER: <ICowGirlText>{
			DESCRIPTION: `The Mooer was dubbed as such due to them being the most basic of $translation:info.type$ that appeared after the incident. Docile, slow and particularly stupid they make up the bulk of the changed. Easy to fool, easy to trick, easy to outrun, but do not get cornered though as they make up for their weaknesses in pure stubbornness and raw strength. Often found fondling themselves, other Mooer's or in very rare cases doing something actually productive. Do not bump into them blindly, check rooms and they shouldn't be a problem right?`,

			WINTEXT: `As the Mooer hits the ground with a thud you feel a wave of relief wash over you. This time at least. You were not going to fall to such a dumb bovine beast. You have kept your humanity for now. But for how long? Next time you might not be able to fend off such a creature.`,
			LOSETEXT: `With a thump the Mooer knocks you to the ground.You try to move but you cannot, your strength beaten out of you. The Mooer quickly pins you down Letting out a long loud Moo of victory it gropes her breasts as a trickle of milk escapes them.You continue to struggle against her, but her pinning is too strong.You can feel her body shift slightly.A few more pushes and you might be able to escape.The Mooer meanwhile has other plans.  Suitably lactating she moves her milk soaked breasts down towards your lips. Struggle as you might she soon has your lips pressed against her milky teat.With a tiny amount of force she pierces your lips and floods the inside of your mouth with her creamy, warm and oh so sweet milk.You relish the taste for a moment. That is until you regain your senses.Drinking the milk is very very bad.Using all the energy you have you grab a hold of the Cowgirl and throw her off you. Milk streaming from her teats as you do so.She is barely dazed and is already moving back to pin you down. You rush to your feet and head off into the cities darker alleys.You eventually lose her as even in your weakened state her movements are too slow and cumbersome to catch you.Panting and gasping for air, your body aches as you remember just what you did.`,
		},

		MOOERUDDER: <ICowGirlText>{
			DESCRIPTION: `The Mooer was dubbed as such due to them being the most basic of Cowgirl that appeared after the incident. Docile, Slow and particularly stupid they make up the bulk of the changed. Easy to fool, easy to trick, easy to outrun do not get cornered though as they make up for there weaknesses in pure stubbornness. Often found fondling themselves, other Mooer's or in very rare cases doing something actually productive. Do not bump into them blindly, check rooms and they shouldn't be a problem right?`,

			WINTEXT: `As the Mooer hits the ground with a thud you feel a wave of relief wash over you. This time at least. You were not going to fall to such a dumb bovine beast. You have kept your humanity for now. But for how long? Next time you might not be able to fend off such a creature.`,
			LOSETEXT: `With a thump the Mooer knocks you to the ground.You try to move but you cannot, your strength beaten out of you. The Mooer quickly pins you down Letting out a long loud Moo of victory it gropes her breasts as a trickle of milk escapes them.You continue to struggle against her, but her pinning is too strong.You can feel her body shift slightly.A few more pushes and you might be able to escape.The Mooer meanwhile has other plans.  Suitably lactating she moves her milk soaked breasts down towards your lips. Struggle as you might she soon has your lips pressed against her milky teat.With a tiny amount of force she pierces your lips and floods the inside of your mouth with her creamy, warm and oh so sweet milk.You relish the taste for a moment. That is until you regain your senses.Drinking the milk is very very bad.Using all the energy you have you grab a hold of the Cowgirl and throw her off you. Milk streaming from her teats as you do so.She is barely dazed and is already moving back to pin you down. You rush to your feet and head off into the cities darker alleys.You eventually lose her as even in your weakened state her movements are too slow and cumbersome to catch you.Panting and gasping for air, your body aches as you remember just what you did.`,
		},

		MILKER: <ICowGirlText>{
			DESCRIPTION: `The Milker is similar to the Mooer hence the similar name. The major difference however is that they seem to facilitate the food production role of the changed. When infected these "Lucky" few grow more than their fair share in breasts and udders. Filling with magnitudes more milk than the standard Mooer. These are never seen on their own and are always around with other Mooers or the more dangerous types of changed. As always avoid Milk as much as possible especially Milker's as theirs seems to be particularly potent. They are however very weak and share the same flaws of the Mooers, slow, dumb and unobservant, however compared to the Mooers they can be physically fought off without any issue, just as long as you don't let them surprise you or drink any of their milk.`,

			WINTEXT: `With one solid hit milk spews all over the ground and straight up in the air, you dodge out of the way with little issue. The defeated Milker lies on the ground squirming ever so slightly as milk continues to leak from its engorged chest and udder. You can already hear the sound of other changed heading your way, drawn by the sweet smell of that milk.As such you quickly finish what you were doing and get out of there.`,
			LOSETEXT: `The milker is just too much for you, despite your best efforts a sliver of milk from her udder seeps through the gap between your lips, the sweet, creamy milk dancing upon your taste buds, your mind begs for more as a rush of endorphins hits your body. In your lapse of concentration you feel a warm fleshy sack press against your lips, it's the Milkers udder! A teat leaking a trickle of that sweet delicious milk. You greedily latch on to it, filling your mouth with her warm fleshy squishy teat. Every suckle torrenting warm delectable milk into your mouth and down your throat, you can feel your stomach fill with the liquid and slowly your lust for the milk is sated. Leaving your mind back to your own. 
	
			Your own...your own milk filled, teat filled mouth!Of the milkers transformative milk!
	
			You gather what little strength your adrenaline can muster and force the milker off.In her pleased milked state she puts up almost no resistance, squirming on the ground as milk leaks from her breasts and teats.The creamy taste fills both your mouth and nose but you keep your will long enough for you to stumble away from the milker and towards your safe house.
	
			But you can feel the milk coursing within you, and sure enough your body begins to change.`,
		},

		HORNCOW: <ICowGirlText>{
			DESCRIPTION: `This unusual changed constantly struts around in a slow and deliberate manner, as if trying to show off its body at all times. Its body is suited for this task with its smooth supple curves, large breasts (dripping with milk) and large jiggly rear. Unlike the regular Mooers these seems to have a drive (Along with a more athletic looking body) but are constantly moaning and giggling, groping their breasts and looking at your genitals whenever they are not directly fighting you.
	
			What you can tell though, is that it\'s feminine slit is consantly leaking a trickle of cum, along with its sensual moans it must be particularly horny.Some sort of Horny Cow version of Changed ? A HornCow ?`,

			WINTEXT: `To not a huge surprise the slim HornCow quickly falls to the ground under your blows. Despite this she seems to be busy groping her body, one hand mashing together her voluptuous breasts, the other peeking its way into her slit. It is hard to tell if she is defeated, or simply ignoring you to pleasure herself. Either Way her distracted state allows you to finish your tasks with ease.`,
			LOSETEXT: {
				MALE: `The HornCow sweeps your knee with her leg and fumbles you to the floor with a loud smack. Before you can regain your balance she is clambering on top of you. Her soft supple hands running down your torso and towards your crotch. You feel her hands delve through whatever pants you have and tear their way towards your cock. With her prize in hand you feel a paralyzing sensation flow through your body as a sweet smell infiltrates your mouth and nose. A splattering of milk, leaking from her breast seeps it's way in, sweet and more sweet it is not as creamy as you expected milk to be (Based on your previous experience with Cows milk) as if it was almost all sugar. It tastes delicious and your body begs for more which is soon delivered. Using one hand to squeeze her breast and the other to pump your cock you are lost to bliss.
	
				Then you feel something warm and wet engulf your cock, moving your attention away from the sweet milk (Although it still lingers heavily in your mind) to your cock, on it is the Horncow, rising your dick up and down.You are overcome with pleasure, the warm wet insides of the Horncow bringing your cock quickly to it's fully erect state, as if naturally you begin to thrust, sinking her body deeper onto your cock. Her loud moans and moo's fill the air as you reach ever closer to a climax.
	
				With one large powerful instictal thrust, you cum inside the HornCow. A loud and sensual moo escaping her lips as a state of euphoria washes over you. In this state of pure thinking, with your mind no longer clouded by petty problems. You realize what you are doing!In her orgasmic state you thrust the Horncow off of your cock, cum (both yours and hers) leaking onto the ground as you leap to your feet and rush off back away from the quivering mooing pile of flesh. You race back to your safe house, but half way there you feel your body feel strange. A heat swells from inside your stomach.It must be the milk…`,
				FEMALE: `The HornCow sweeps your knee with her leg and fumbles you to the floor with a loud smack. Before you can regain your balance she is clambering on top of you. Her soft supple hands running down your torso and towards your crotch. You feel her hands delve through whatever pants you have and tear their way towards your slit. With her prize in hand you feel a paralyzing sensation flow through your body as a sweet smell infiltrates your mouth and nose. A splattering of milk, leaking from her breast seeps it's way in, sweet and more sweet it is not as creamy as you expected milk to be (Based on your previous experience with Cows milk) as if it was almost all sugar. It tastes delicious and your body begs for more which is soon delivered. Using one hand to squeeze her breast and the slip a finger into your slit.
	
				Your body is wracked with a deep and intense pleasure as her finger begins to explore your vaginal wall.Digging deeper with each second, your body crying out as her warm digit penetrates your even warmer folds.All the while she continues to fill your mouth full of her milk, you struggle to know which to focus on, the lovely sweet milk filling your mouth. Or the intense exploration from below.
	
				Then as sloppily as she piece your insides she removes her finger and finishes filling your mouth with her morish milk. She stands up and giggles at you as you squirm to regain your composure.With one long sultry but quiet moo she wanders off, her ass deliberately shaking from side to side as she walks, as if teasing you with a butt you want to grope badly.
	
				After a few minutes of calming your body down you stumble to your feet, luckily no Changed found you in your horny and debilitated state.As you head back to your safe house to rest from your encounter, your previous task now too dangerous to continue, you feel your body begin to feel funny. It's the milk!`,
			},
		},

		HERMCOW: <ICowGirlText>{
			DESCRIPTION: `A Hermaphrodite? That\'s what this is, at least you're pretty sure, her body generally a little taller than all of the other Changed you have seen wandering around. Her hair a striking red, stomach toned and arms ripped with more muscle than many combined. But she still retains a somewhat chubby body, just more muscular than the rest. 
	
			The most important difference is the 11 or 12 inch cock fully erect and sticking out from her crotch.She eyes you with a wide smirk, eyes piercing into your body, not ones of pity or anger.Ones that you can only describe as, Domineering.`,

			WINTEXT: `You sap the last bits of energy from the Herm Changeds body, topping over with a loud \'Thud\' you can see its breathing calm and its cock stay somehow still erect. Its eyes begin to flicker, gently opening a few times showing signs of the Changed waking up, you best finish whatever you were doing quick before it comes back to claim you…`,
			LOSETEXT: {
				MALE: `You feel the last bit of strength in your body fade, your legs bucking from under your weight Slamming ass first onto the ground you watch as the Hermcow saunters towards you, Her cock filling your vision as the large member is placed before your head. A sweet and intoxication aroma spills from the tip, filling your senses. 
	
				Then suddenly a warm liquid hits your face.You are brought back from the intoxication aroma for but a moment.Its effects still weighing heavily on your senses and mind.You lick your lips and are greeted to a warm creamy sweet taste that instantly makes you crave for more.And the Hermcow is more than willing to oblige.Using your moment of absent mindedness she pins you to your back with her strength, her D cup breasts pushing against your lips, a trickle of milk leaking from her nipple and through the gaps of your more than willing mouth.The inside soon awash with the sweet creamy taste of her milk.You cannot help yourself but to lift your head to latch onto her nipple, sucking it gently your mouth is soon rewarded with a torrent of milk as the hermcow moo\'s in pleasure.
	
				This goes on for a minute, until her milk begins to run dry.With one long reluctant moo the Hermcow pushes you off her breast and stands over you.With a cackling chuckle she turns around, giving you a full view of her rear before walking off back towards the streets.
	
				It seems she got her use out of you.But her face seemed solemn.As if she was disappointed about something. As your mind returns to your own control (And not controlled by lust and pleasure) you can feel your body begin to change… perhaps for good ?`,
				FEMALE: `You feel the last bit of strength in your body fade, your legs bucking from under your weight Slamming ass first onto the ground you watch as the Hermcow saunters towards you, Her cock filling your vision as the large member is placed before your head. A sweet and intoxication aroma spills from the tip, filling your senses and heating your body, your crotch flaring up with need and your breasts crying out to be touched. 
	
				Then suddenly a warm liquid hits your face.You are brought back from the intoxication aroma for but a moment.Its effects still weighing heavily on your senses and mind.You lick your lips and are greeted to a warm creamy sweet taste that instantly makes you crave for more.Its milk!Direct from her breast straight onto your face, you shouldn't be drinking it but it tastes so nice… In your addled state you can just barely see the Hermcow tear away any clothing covering your crotch. Your slit exposed to the cool air of the city is not left unattended for long. The Hermcow repositions her cock, pressing the tip gently against your slit with a warm and pleasant feeling.The intoxicating smell and sweet milk has sent your body into overdrive, your slit leaking a trickle of cum and begging for use.
	
				An orgasmic feeling courses through your body as the hermcow thrusts her cock into you, the girth of the member just barely able to fit inside.You can feel every inch of your vagina become filled with her warm dripping cock.Again and again she thrusts, shaking your body as her cock inches deeper within you.Pleasure mounting within you as you reach the cusp of an orgasm, all the while the Hermcow simply smiles and thrusts.Just as you were about to orgasm your insides are filled with warm and fulfilling liquid bringing you to climax over the Hermcows cock.Your womb and vagina now filled she sloppily pulls her cock from out of you, cum from both of you escaping your slit as it is emptied.
	
				She stands over you, sliding her hand down her soaked cock with a chuckle.Watching as you squirm in pleasure on the floor, cum leaking from you.She then turns around, as a horde of Mooers wander in from the street, no doubt hearing or smelling the \'event\' happening between you and the hermcow. But… she leads them off, this is repeated constantly while you are able to recover yourself.
	
				As you begin to leave she turns to you and winks, before stepping off down an alleyway, gone out of sight. Before you can contemplate what happened you can feel your body begin to change before your eyes...`,
			},
		},
	},

	VICTORY: {
		MOOER: [
			`As the Mooer hits the ground with a thud you feel a wave of relief wash over you. This time at least. You were not going to fall to such a dumb bovine beast. You have kept your humanity for now. But for how long? Next time you might not be able to fend off such a creature.`,
			`Beaten, the Mooer rushes off to either recover its wounds or get help. Either way, you are victorious and are no long in immidiate danger. But you best be off in case others heard your fight, or it comes back for another round.`,
		],
		MILKER: `Spewing milk all over the ground and straight up in the air you dodge out of the way with little issue. The debeated Milker lies on the ground squirming ever so slightly as milk continues to leak from its engorged chest and udder. You can already hear the sound of other changed heading your way, drawn by the sweet smell of that milk.`,
	},
	LOSS: {
		DEFAULT: `With a thump the Mooer knocks you to the ground.You try to move but you cannot, your strength beaten out of you. The Mooer quickly pins you down Letting out a long loud Moo of victory it gropes her breasts as a trickle of milk escapes them.You continue to struggle against her, but her pinning is too strong.You can feel her body shift slightly.A few more pushes and you might be able to escape. The Mooer meanwhile has other plans. Suitably lactating she moves her milk soaked breasts down towards your lips. Struggle as you might she soon has your lips pressed against her milky teat.With a tiny amount of force she pierces your lips and floods the inside of your mouth with her creamy, warm and oh so sweet milk. You relish the taste for a moment. That is until you regain your senses.Drinking the milk is very very bad. Using all the energy you have you grab a hold of the Cowgirl and throw her off you. Milk streaming from her teats as you do so.She is barely dazed and is already moving back to pin you down. You rush to your feet and head off into the cities darker alleys.You eventually lose her as even in your weakened state her movements are too slow and cumbersome to catch you. Panting and gasping for air, your body aches as you remember just what you did.`,
		HOSPITAL: `The Enemy lands one solid hit on you and you fall to the ground. You can hear her and the other moo's slowly approach you...`,
	},

	CHEATS: {
		STOPINFECTIONPROCESS: `Either on purpose or by accident you have swallowed another sizeable amount of milk.
		
		To your surprise, you don't feel anything changing. You might be safe, for now.`,
	},
	// FIGHT: { PLAYERMISS: `` } // Moved to encounter.ts
}
