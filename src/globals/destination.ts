export enum DestinationDanger {
	NONE = 0,
	LOW = 1,
	MEDIUM = 2,
	HIGH = 3,
}
export enum DestinationSupplies {
	NONE = 0,
	VARIED = 1,
	FOOD = 2,
}
export enum DestinationUnlock {
	OUTSKIRT = 1,
	HOSPITAL = 2,
	PART_ANTENNA = 4,
	PART_GENERATOR = 8,
	PART_RADIO = 16,
}
export type TDestinationUnlock = 'OUTSKIRT' | 'HOSPITAL' | 'PART_ANTENNA' | 'PART_GENERATOR' | 'PART_RADIO'
export const DestinationUnlockLookup: { [key in TDestinationUnlock]: number } = {
	OUTSKIRT: 1,
	HOSPITAL: 2,
	PART_ANTENNA: 4,
	PART_GENERATOR: 8,
	PART_RADIO: 16,
}
export interface IDestination {
	Display: string
	Danger: DestinationDanger
	Supplies: DestinationSupplies
	Description: string
	Unlock?: DestinationUnlock | DestinationUnlock[]
}

export type TDestinations = 'Home' | 'Outskirts' | 'CityCentre' | 'Park' | 'Hospital' | 'Office'

export const DESTINATIONS: { [key in TDestinations]: IDestination } = {
	Home: {
		Display: 'Home',
		Danger: DestinationDanger.NONE,
		Supplies: DestinationSupplies.NONE,
		Description: `Your home. Or perhaps put the roof of your home. A place you have managed to secure after the initial confusion that seems out of reach for the Changed. Spending a day here will get you some much needed rest. But will still consume food`,
	},
	Outskirts: {
		Display: 'Outskirts',
		Danger: DestinationDanger.LOW,
		Supplies: DestinationSupplies.VARIED,
		Description: `The Outskirts of the city. While most of the changed went off into the city center, some have remained here. The danger is lower but so are the supplies. But no doubt you will find something useful here, and maybe a path deeper.`,
	},
	CityCentre: {
		Display: 'City Centre',
		Danger: DestinationDanger.MEDIUM,
		Supplies: DestinationSupplies.VARIED,
		Description: `Finding a secluded safe path into the city centre has allowed you access to more resources, however it is not without its risks as both the changed are more numerous and the more dangerous strains roam these parts.`,
		Unlock: DestinationUnlock.OUTSKIRT,
	},
	Park: {
		Display: 'Park',
		Danger: DestinationDanger.MEDIUM,
		Supplies: DestinationSupplies.FOOD,
		Description: `The park. A place that is constantly oozing with the Changed. But in such a dangerous area, the rewards could be great.`,
	},
	Hospital: {
		Display: 'Radio',
		Danger: DestinationDanger.HIGH,
		Supplies: DestinationSupplies.NONE,
		Description: `You have the radio, power, and the arial. Now you just have to use it and hope that someone out there is listening...
		
		There is no turning back once you use the radio.`,
		Unlock: [DestinationUnlock.PART_ANTENNA, DestinationUnlock.PART_GENERATOR, DestinationUnlock.PART_RADIO],
	},
	Office: {
		Display: 'Marked Building',
		Danger: DestinationDanger.HIGH,
		Supplies: DestinationSupplies.NONE,
		Description: `The building the strange Mooer marked out after your conversation. Is it work the risk to investigate?`,
		Unlock: DestinationUnlock.HOSPITAL,

		// [DIALOGUE] -> "To the Hospital!"
	},
}
