import { useGlobalStore } from '../store/global'
import { TSkill } from './entity'

export const COWGIRL = ['mooernoudder', 'mooerudder', 'milker', 'horncow', 'hermcow'] as const
export type TCowGirl = typeof COWGIRL[number]
export const CowGirlFriendlyString: { [key in TCowGirl]: string } = {
	mooernoudder: 'Mooer',
	mooerudder: 'Mooer',
	milker: 'Milker',
	horncow: 'HornCow',
	hermcow: 'HermCow',
}

export interface ICowGirlStats {
	damage: number
	health: number
	experience: number
	attackstat: TSkill
	defense: number
	strength: number
	intelligence: number
	agility: number
	stamina: number
	perception: number
}

export interface ICowGirl {
	type: TCowGirl
	stats: ICowGirlStats
}

export function CreateCowGirl(type?: TCowGirl) {
	const globalStore = useGlobalStore()

	let forced = true
	if (!type) {
		forced = false

		let cw = COWGIRL as unknown as string[]
		if (!globalStore.hermKink) cw = cw.filter((x) => x !== 'hermcow')

		type = COWGIRL[Math.floor(Math.random() * cw.length)]
	}

	if (forced && type === 'hermcow') {
		if (!globalStore.hermKink) {
			console.error('Event tried to force a hermCow despite the disabled kink! Giving mooernoudder instead.')
			type = 'mooernoudder'
		}
	}

	// Change according to kink
	if (type === 'mooernoudder' || type === 'mooerudder') {
		type = globalStore.udderKink ? 'mooerudder' : 'mooernoudder'
	}

	type = 'mooernoudder'

	return <ICowGirl>{
		type,
		stats: JSON.parse(JSON.stringify(COWGIRLS[type])),
	}
}

export const COWGIRLS = <{ [key in TCowGirl]: ICowGirlStats }>{
	mooernoudder: {
		damage: 2,
		health: 2, // maybe we can set this to a higher value in the future when we solve out game balance issues
		experience: 5,
		attackstat: 'strength',
		defense: 12,
		strength: 2,
		intelligence: 1,
		agility: 1,
		stamina: 3,
		perception: 1,
	},

	mooerudder: {
		damage: 2,
		health: 2,
		experience: 5,
		attackstat: 'strength',
		defense: 12,
		strength: 2,
		intelligence: 1,
		agility: 1,
		stamina: 3,
		perception: 1,
	},

	milker: {
		damage: 2,
		health: 2,
		experience: 5,
		attackstat: 'strength',
		defense: 12,
		strength: 2,
		intelligence: 1,
		agility: 1,
		stamina: 3,
		perception: 1,
	},

	horncow: {
		damage: 2,
		health: 2,
		experience: 5,
		attackstat: 'strength',
		defense: 12,
		strength: 2,
		intelligence: 1,
		agility: 1,
		stamina: 3,
		perception: 1,
	},

	hermcow: {
		damage: 2,
		health: 2,
		experience: 5,
		attackstat: 'strength',
		defense: 12,
		strength: 2,
		intelligence: 1,
		agility: 1,
		stamina: 3,
		perception: 1,
	},
}
