export const SKILLS = ['strength', 'intelligence', 'agility', 'stamina', 'perception'] as const
export type TSkill = typeof SKILLS[number]

export function attackEntity(skillValue: number, defenseValue: number): [false | number, number, number] {
	const roll = Math.floor(Math.random() * 20) + skillValue
	return [roll >= defenseValue ? Math.abs(defenseValue - roll) + 1 : false, roll, defenseValue]
}
