import { useGlobalStore } from '@/store/global'
import { usePlayerStore } from '@/store/player'
import { useEncounterStore } from '../store/encounter'
import { ICowGirl } from './cowgirl'
import { DestinationUnlock, TDestinations } from './destination'
import { TSkill } from './entity'

export const ENCOUNTEREMITS = [
	'hide-encounter',
	'show-transition',
	'finish-day', // -> shortcut for both
	'finish-game', // -> Game Over
] as const
export type TEncounterEmits = typeof ENCOUNTEREMITS[number]

export interface IEncounterOptionClick {
	emit: (type: TEncounterEmits) => void
	store: ReturnType<typeof useEncounterStore>
	gStore: ReturnType<typeof useGlobalStore>
	grabText: (key: string) => string
}
interface IEncounterBaseOption {
	title: string
}
export interface IEncounterOption extends IEncounterBaseOption {
	click: (options: IEncounterOptionClick) => any
}
export interface IEncounterOptionRoll extends IEncounterBaseOption {
	type: TSkill
	success: ((options: IEncounterOptionClick) => any) | string | undefined
	fail: ((options: IEncounterOptionClick) => any) | string | undefined
}
interface IEncounterContent {
	[key: string]: string | IEncounterContent
}
export interface IEncounter {
	title: string
	content: {
		DEFAULT: string
		[key: string]: string | IEncounterContent | undefined
	}
	rewards?: IEncounterResult[]
	failure?: IEncounterResult[]
	difficulty?: number
	reachable?: boolean
}
export interface ISimpleEncounter extends IEncounter {
	options: (IEncounterOption | IEncounterOptionRoll)[] | 'none'
}
export interface IFightEncounter extends IEncounter {
	cowgirl: ICowGirl

	onWin?: (options: IEncounterOptionClick) => any
	onLose?: (options: IEncounterOptionClick) => any
}

type TEncounterType = 'simple' | 'fight'
export function getEncounterType(obj: any): TEncounterType | null {
	if ('options' in obj) return 'simple'
	if ('cowgirl' in obj) return 'fight'
	return null
}

export type IEncounterResult = DestinationUnlock | IEncounterResultStat
interface IEncounterResultStat {
	reward: TEncounterReward
	amount: number
}
type TEncounterReward = 'none' | 'experience' | 'food' | 'materials' | 'health'

interface IEncounterGuide {
	UNIQUE?: ISimpleEncounter[]
	MUNDANE: ISimpleEncounter[]
}

export const ENCOUNTERS: { [key in TDestinations]: IEncounterGuide } = {
	Home: { UNIQUE: [], MUNDANE: [] },

	Outskirts: {
		UNIQUE: [
			{
				title: 'A Survivor',
				content: {
					DEFAULT: `"No get away!" You hear the shouts of a man running through the suburban streets screaming at the top of his lungs. A bad move as it will no doubt draw more attention. Well running is not exactly the best word to describe it. Limping is more like it, his foot drags sideways across the ground as a Mooer follows close behind, waddling with her docile walk, cupping her breasts as she squirts milk towards him. It\'s a pitiful sight, the busty hucow will get to him very soon as the way he is injured is making him much slower than her and he will be a Mooer for sure. Should you help him?`,
					SUCCESS: `Luckily for him you manage to lure or “persuade” the Mooer away much to his relief. As you approach him he thanks you greatly before thrusting some dried food into your hands then limping away. As you watch him slowly go out of sight you cannot help but feel proud of yourself that you helped someone this day. One less Mooer might not make much of a difference, but the Changed were all people once, maybe one day that can be fixed.`,
					FAIL: `Despite your best efforts you cannot prevent the inevitable. With a trip he falls to the ground, the Mooer soon on top of him forcing her breast into his mouth, milk rushing in to fill his empty mouth with her creamy sweet milk. He squirms and squirms but soon stops, his chest ballooning out as two breasts begin to poke out through them, growing from A to B to C showing no sign of stopping. His squirming stops, replaced with moans and suckling of her teat, a sack pokes out between his jeans and with that he is too far gone. You lick your wounds and head off, at least she is not giving you her special 'Treatment' but you sure did expend a lot of effort for nothing.`,
				},

				difficulty: 12,
				rewards: [
					{
						reward: 'experience',
						amount: 2,
					},
					{
						reward: 'food',
						amount: 2,
					},
				],
				failure: [
					{
						reward: 'health',
						amount: 2,
					},
				],

				options: [
					{
						title: 'Fight the Mooer off',
						type: 'strength',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Distract the Mooer with a makeshift device',
						type: 'intelligence',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Fight the Mooer off',
						type: 'stamina',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Throw things to distract the Mooer, or at least slow it down.',
						type: 'perception',
						success: undefined,
						fail: undefined,
					},
				],
			},
			{
				title: 'Cornered',
				content: {
					DEFAULT: `While rummaging through an open house you hear the sounds of mooing come from behind.As you turn around you spot a Mooer.Just a regular plain old one standing in the opening of the door to the outside.There is no way around it, you will have to fight your way out.The stuff you have found in this house is too good to give up, and you can deal with just one right ?`,
					SUCCESS: `Falling to the ground the the Mooer faints from exhaustion. It won't be long however till they are back up and wanting to change you. Hurriedly collecting your spoils from the scavenge you rush off with your supplies and food. Time well spent because you won, but it could have gone poorly.`,
				},

				options: [
					{
						title: 'Fight!',
						click: ({ store, grabText }) => {
							store.setEncounter(
								store.createFightEncounter(undefined, grabText('success'), [
									{
										reward: 'materials',
										amount: 2,
									},
									{
										reward: 'food',
										amount: 2,
									},
								]),
								true
							)
						},
					},
				],
			},
			{
				title: 'Blockade',
				content: {
					DEFAULT: `Only moments after you leave your sanctuary you are blocked by a herd of Changed. Mooing, fondling each others breasts and some suckling each others teats they are doing typical Changed things. However they are blocking the way in, you need to figure out a way past them and hopefully without getting caught.`,
					FAIL: `Your plan does not succeed. You accidentally trigger the attention of the horde and before you know it they are all looking right at you and heading your way. Breasts jiggling and udders leaking. You have to flee. As you turn a corner into an alley you are hit face first by the sight of a Mooer standing in your way. You can hear the herd slowly approaching behind you. You need to fight your way through this one or get swarmed by hundreds.`,
					SUCCESS: `While such a herd can be imposing, they are still the simple Changed you have come to know. Stupid and gullible you make it past them without wasting too much time and learn a few things about them as a result. Now hopefully you can get to doing something more productive tomorrow as it took all day to move them. At least you have your humanity still.`,
				},

				difficulty: 12,
				rewards: [
					{
						reward: 'experience',
						amount: 10,
					},
				],

				options: [
					{
						title: 'Try and lure them away by creating a noise a few streets down.',
						type: 'intelligence',
						success: undefined,
						fail: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterFight(grabText('fail'), ({ store }) =>
									store.setEncounter(store.createFightEncounter(), true)
								)
							)
						},
					},
					{
						title: 'Sneak past the hucow horde',
						type: 'agility',
						success: undefined,
						fail: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterFight(grabText('fail'), ({ store }) =>
									store.setEncounter(store.createFightEncounter(), true)
								)
							)
						},
					},
					{
						title: 'Watch their patterns and wait for a gap to squeeze through',
						type: 'perception',
						success: undefined,
						fail: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterFight(grabText('fail'), ({ store }) =>
									store.setEncounter(store.createFightEncounter(), true)
								)
							)
						},
					},
				],
			},
			{
				title: 'Signs of Intelligence',
				content: {
					DEFAULT: `Upon your travels you come across the now mundane sight of a herd of changed, wandering around confused, mooing and pleasuring each other in brazen fashion. Luckily they are not in your way. Just as you are about to leave though a changed you have never seen before walks out of the herd in-front of the twenty Mooers. She looks almost identical to the standard Mooer, but her walk is much more dignified than the others. While the others walk with no grace, purposely showing off their body as it jiggles this Mooer does not. It turns around to face the others, all lost in there own little worlds of whatever Mooers think. The strange Mooer lets out a loud moo and all the others stop immediately, turn to face the strange Mooer and stare at her. She moo's once again pointing with $translation:a stubby finger|soft dainty hands$ towards the City Centre. The herd Moo's in unison before smiling and waddling off in the direction of the city centre. They listened to whatever she said, at least that's what it looked like. If they start to organise this could quickly become an issue for your humanity, at least you learnt about this now rather than before it surprised you in a tricky situation.`,
				},

				rewards: [
					{
						reward: 'experience',
						amount: 10,
					},
				],

				options: 'none',
			},
			{
				title: 'A Way In',
				content: {
					DEFAULT: `Walking past one of the subway entrances you notice the metal grate that had barred entry has been lifted slightly. No doubt when the power went down the mechanism holding it became pliable to force. The handwork looks to be of a person rather than one of the changed. And with no sign of milk it is probably a safe bet to guess it was humans hands that did this. If so. This could provide a safe way into the inner city. Making sure to mark this on your map you head inside. It takes a while but you manage to find a non blocked passageway into the city centre. It will be more dangerous for sure. But it will also provide a much more varied bounty of supplies and goods. Today may be over. But Tomorrow, this could be the breakthrough you need to survive.`,
				},

				rewards: [
					{
						reward: 'experience',
						amount: 10,
					},
					DestinationUnlock.OUTSKIRT,
				],

				options: 'none',
			},
		],
		MUNDANE: [
			{
				title: `A Useful Day's Scavenge`,
				content: {
					DEFAULT: `Other than the odd lone Mooer you have had a rare uneventful day. While both food and Materials were running a little dry. Today you manage to scavenge enough of both from random (Already looted) homes and businesses around the cities outskirts near your base to get enough to keep you steady. But for how long is your major concern. It is unlikely tomorrow will go as tepid as today did.`,
				},

				rewards: [
					{
						reward: 'materials',
						amount: 2,
					},
					{
						reward: 'food',
						amount: 2,
					},
				],

				options: 'none',
			},
			{
				title: 'Deserted House',
				content: {
					DEFAULT: `A typical site nowadays. A house left abandoned, door wide open with the stains of milk smattering the walls inside. No doubt someone was caught here and fought but eventually succumb, the tattered remains of clothing on the floor giving prudence to this theory. Lucky for you though the house is still full of supplies, after just a few minutes of scavenging you have enough food to last a couple days. Once you try to leave however. A Mooer has snuck in from behind!`,
					SUCCESS: `The Mooer successfully dealt with allows you take your spoils and escape with no issue. It seems as the days go on, the rate of Mooers increase, and they were pretty numerous to begin with.`,
				},

				options: [
					{
						title: 'Fight!',
						click: ({ store, grabText }) => {
							store.setEncounter(
								store.createFightEncounter(undefined, grabText('success'), [
									{
										reward: 'food',
										amount: 2,
									},
								]),
								true
							)
						},
					},
				],
			},
		],
	},
	CityCentre: {
		UNIQUE: [
			{
				title: 'A Preserved Cafe',
				content: {
					DEFAULT: `Heading through the Inner city you spot a wide open door leading into a rather well kept cafe interior. Despite the intrusion the doorway is almost immaculate. The lock on the door has been picked open not forced open. Perhaps that means there could be supplies inside? Should you risk going into an enclosed space?`,
					FAIL: `Heading into the building without proper planning you end up stumbling, crashing into the chairs and tables causing a cacophony of sound to echo out into the street. As luck would have it a small herd of the changed were walking past. They quickly descend upon the source of the noise. You!`,
					SUCCESS: `Successfully entering the cafe with no qualms thanks to your planning or careful movements. A small herd walks past the entrance, but you manage to deftly avoid them and they soon pass. Searching the premise you find inside a bedroom stained with milk all over the Bed. You find a carton of the Infected milk beside the bed. No doubt they had some before enjoying the true effects of the milk. Despite this the only other sign of milk you could find was downstairs in a pantry that had been ransacked hastily by someone. The milk had stained the floor around it scattered boxes of someone in a rush. Still the pantry was filled with non-perishable goods. More than enough for you to fill your bags full. More intriguingly though you find a fully functioning Ham Radio. Or at least it would be if it had power and an aerial. With this you might be able to contact the outside world and get help. Or even contact someone within the city. There must be someone not mooing out there...`,
				},

				rewards: [
					{
						reward: 'food',
						amount: 2,
					},
					DestinationUnlock.PART_RADIO,
				],
				difficulty: 14,

				options: [
					{
						title: 'Scout the Area',
						type: 'intelligence',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Sneak inside',
						type: 'perception',
						success: undefined,
						fail: undefined,
					},
				],
			},
			{
				title: 'A Series of Generators',
				content: {
					DEFAULT: `Dipping into one of the market halls to avoid a patrol of Mooers, lead by some strange Mooer way out in front, walking as if they have a purpose and not simply fondling every $translation:info.type$ they see, you stumble upon a fantastic sight. Generators! Power! A generator stall, previously supplying generators to farmers or builders for agricultural/building work. You could really use one of these. They might make a lot of noise, but connect it to a battery on a roof somewhere they cannot get to and you can have all the power you need. But you can hear the Mooers patrolling. Any noise would alert them to your presence. You must figure out which ones work and which don't without turning them on. There is no way you could carry them all with you.`,
					FAIL: `Adjusting a few wires and tugging in one spot, The generator bursts into life! Yes it works! The loud sputtering generator fills your ears and the market hall with noise. The noise is soon overshadowed however by the sound of marching Mooers coming in to investigate the noise. By the sound of it, far too many to fight. With little choice and not enough time to turn off the generator, you have to run. At least it will be a good distraction. You'll just have to come back for the generator another day.`,
					SUCCESS: `Tank is full. Wiring seems to be all in place. Looks like it's good to go and you made no noise. Lifting the generator, there is no way you will be able to take anything else with you. But power, power would be nice to have. You are certain there is something useful you could do with this.`,
				},

				rewards: [
					{
						reward: 'experience',
						amount: 20,
					},
					DestinationUnlock.PART_GENERATOR,
				],
				difficulty: 16,

				options: [
					{
						title: 'Figure out mechanically which would work',
						type: 'intelligence',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Look for anything of note around the generators to prove which works',
						type: 'perception',
						success: undefined,
						fail: undefined,
					},
				],
			},
			{
				title: 'Scrap Pile',
				content: {
					DEFAULT: `As you head into the City Centre, you see a large pile of scrap electronics abandoned (or dumped) right outside a completely ransacked home appliances store. Windows smashed, shelves once full of TV's, dishwashers and more, now only wanton destruction remains. Either way, the pile of goods may have something useful inside for a person with a keen eye or a sturdy body.`,
					FAIL: `You chip away at the pile of random jagged and tangling wires. Anything useful you did find you ended up breaking in the removal process. And even then you only ever found pitiful amounts. Eventually you have to call it quits as you are unable to attain anything from this other than a few cuts from sharp jagged metal, and sprained muscles.`,
					SUCCESS: `Able to successfully separate useful scrap from useless scrap. Reusable parts from completely broken parts and pointless (And scrap!) metal you collect a surprisingly valuable haul from what seemed to be the dumping of supposedly useless goods. Though perhaps when the place was looted these supplies were not in need and are only now useful in the moopocalypse. Among the supplies you find a large radio antenna buried right at the bottom of the pile. Something that could be very useful if you can find the right parts to go with it. Communication since the internet went down has been hard. A fully functional radio could fix that. Now to just safely get your supplies back to your base, which should be easy thanks to your shortcut.`,
				},

				rewards: [
					{
						reward: 'materials',
						amount: 3,
					},
					DestinationUnlock.PART_ANTENNA,
				],
				failure: [
					{
						reward: 'health',
						amount: 2,
					},
				],
				difficulty: 14,

				options: [
					{
						title: 'Gruelling work like this requires a lot of stamina',
						type: 'stamina',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'A task like this requires a keen eye',
						type: 'perception',
						success: undefined,
						fail: undefined,
					},
				],
			},
			{
				title: 'A Loner $translation:info.type$',
				content: {
					DEFAULT: `The city centre is usually chock full of the Changed. Yet you have wandered for hours now unable to find any. But then you come across a really strange sight. A Mooer sits alone at a table. Her large $translation:info.bovine$ body barely fitting into the plastic chair she is sitting in. Her $translation:fur is a little unusual though. Instead of black and white (the most common type) it's a dark, almost crimson like red with black. Although you have seen some unique colours before (mostly pinks and greys), they have never delved far into the dark|smooth skins reflects the sunlight with a fine oiled shine, this is the first time you’ve ever seen that happen on one of their skins$. It spots your approach and speaks out.
					“Ahh a moooooo... A Human, or are you? An interesting question these days no? I have mooot seen one for quite some time” The $translation:info.type$ moos every so often in between words. Words that take a mooing tone as well, with o and m elongated.
					
					“Doooo not worry Human.I still have my senses.For now at least.” She moo's, a smirk forming across her $translation:detail.face$. “Oooooone of those beasts pinned me down in my home. Force fed me milk. It was delicious and I planned to drink it all. But some strange man came and forced it off me. Before bolting off. It gave me enough time to return to my senses and flee. But not without causing some obvious effects” She giggles, raising a hand to grope her massive E cup breasts. The jiggly flesh bouncing teasingly before your eyes. “Not that I mind too much you know. It feels wonderful. Every touch, every stream of milk. Having my booooody like this and my sanity... It is quite the orgasmic circumstance I assure you.And the milk.Oh I could just drink gallons of the stuff.And I do.Oh it is simply sublime!”She smirks the entire time.Eyes staring at you as she strokes her milk engorged breasts.You can tell she is hiding something. Anyone who drinks as much as she is claiming to have would be mindless in seconds.But what exactly she is hiding and how you could persuade her to give you the information. This might be more difficult than just dealing with Mooers...`,
					FAIL: `I tire of this conversation.” She takes a deep breath before letting out a ear shattering moo. It is only seconds after she finishes that you can hear the sound of hooves against concrete heading your way.
					“This way my dear” She taps her hoof against the tiled patio. A hatch opens beside her leading downwards into a dimly lit tunnel. She beckons you to enter it as the $translation:info.type$ herds converge on your location. They are numerous.No way you could force your way through them.The tunnel is your only option unfortunately. Diving inside she shuts the hatch behind you. Soon after the sounds of her mooing and moaning echo down the tunnel, along with the faint sound of slurping noises.
					The tunnel leads on for a little while before reaching another hatch, left open with light shining through it from the outside. You emerge in an alley. Not a unfamiliar sight these days. Maybe five or six blocks away from where you spoke to the unusual Mooer.As you exit the new hatch it slams shut behind you.You quickly spot a camera whirring as it turns to face you a voice comes from a nebulous direction.
					
					“My you MOOOO YES!” The voice cuts off with a moan and a moo as the sound of giggly and slurping noises echo through the mic. “You were a bit of a disappointment.I was hoping to find someone who was... interesting” She cuts off again with a moan and a moo. “ And not just some mindless Sex slave like the others. Not that I mind those...In fact I'd say you have given me a new perspective. Well my Dear I leave you with one of my best 'Gifts' . Oh and know that if you ever see me again, you will be destroyed. Remember that” The voice fizzles off with the sound of electronic crackles.

					“Mooooooooo!” A noise comes from behind the corner of the Alleyway. This must be that 'Gift' she was mentioning. You are trapped, and must fight your way out if you want to keep your humanity.`,
					SUCCSES: `She looks at you surprised. “My. Quite the smooth talker I will admit. Your attention to detail in moooooooy story is impeccable. Though perhaps it is quite obvious I am not the normal $translation:info.type$ huh?” She giggles out loud, her massive breasts jiggling from her movements. “I won't spoil too much. All you need to know is.I am from the Omnicorp Megamilk company. But I tire of this conversation.” She takes a deep breath before letting out a ear shattering moo. It is only seconds after she finishes that you can hear the sound of hooves against concrete heading your way.
					“This way my dear” She taps her hoof against the tiled patio. A hatch opens beside her leading downwards into a dimly lit tunnel. She beckons you to enter it as the hucow herds converge on your location. They are numerous.No way you could force your way through them.The tunnel is your only option unfortunately. Diving inside she shuts the hatch behind you. Soon after the sounds of her mooing and moaning echo down the tunnel, along with the faint sound of slurping noises.
					
					The tunnel leads on for a little while before reaching another hatch, left open with light shining through it from the outside. You emerge in an alley. Not a unfamiliar sight these days. Maybe two or three blocks away from where you spoke to the unusual Mooer.On a wall near the exit is a small digital screen. Powered!Somehow.On it reads “You were fun to talk to. But I must attend my 'duties'.Hope you do not run into me again HUMAN or you won't be that any more” In the bottom left of the screen is a map. A map of the city! An unassuming office block coloured in bright red. In fact it's not far from the Subway entrance.Dare you go there?`,
				},

				rewards: [
					{
						reward: 'experience',
						amount: 20,
					},
					DestinationUnlock.HOSPITAL,
				],
				difficulty: 14,

				options: [
					{
						title: 'Be blunt and ask directly about the differences between her and the rest',
						type: 'intelligence',
						success: undefined,
						fail: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterFight(grabText('fail'), ({ store }) =>
									store.setEncounter(store.createFightEncounter(), true)
								)
							)
						},
					},
					{
						title: 'Push on certain phrases she speaks, but be civil.',
						type: 'perception',
						success: undefined,
						fail: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterFight(grabText('fail'), ({ store }) =>
									store.setEncounter(store.createFightEncounter(), true)
								)
							)
						},
					},
				],
			},
		],
		MUNDANE: [
			{
				title: 'Return to the Cafe',
				content: {
					DEFAULT: `As you head into the City Centre you come across the Cafe you had previous ventured into. Last time you looked it was stock full of so much food you could not gather even a tenth before having to go. Maybe it is still there? Stepping inside you cannot not smell milk. It's pungent aroma hitting you as soon as you step inside. You reach the pantry but inside. Inside is a Mooer eating some of the food! Some of it is spoiled by the milk escaping her breasts onto it. But most still seems safe. But to get to it. You will need to pacify(Beat up) the Mooer in the way...`,
					SUCCSES: `As the Changed hits the ground with a thud you quickly grab some of the unspoiled food before it wakes up. You manage to fill your bags with around three days worth of food before she starts to awaken. A good haul today. But will it be tomorrow?`,
				},

				options: [
					{
						title: 'Fight!',
						click: ({ store, grabText }) => {
							store.setEncounter(
								store.createFightEncounter(undefined, grabText('success'), [
									{
										reward: 'food',
										amount: 3,
									},
								]),
								true
							)
						},
					},
				],
			},
			{
				title: 'The Scrap Pile',
				content: {
					DEFAULT: `You find yourself back at the scrap pile you found the antenna in. The day is getting late and the pile is filled with useful stuff. You grab some of the more useful looking scrap and head back home. You manage to collect a good amount of very useful metal which you can use for various things. But you did not find any food. At least not today.`,
				},

				rewards: [
					{
						reward: 'materials',
						amount: 2,
					},
				],

				options: 'none',
			},
		],
	},
	Park: {
		UNIQUE: [
			// Unused
			// {
			// title: 'A walk in the park?',
			// content: {
			// DEFAULT: `The local park, previously a maze of dogs, children and flowers, now… well it's still full of flowers and the occasional dog. It seems whatever the milk does to humans does not affect dogs. But what about cows? That question is soon answered. Your wanderings lead you to a large herd of cattle, actual cattle grazing their way through the park’s grass. Interspersed between them are Mooers cuddling and playing with each other for some reason.\n\nThe sight is rather baffling, the cows are docily going along with the Mooers, often being milked by them.Perhaps a sense of companionship ? A strange relative relationship ? Or just these unintelligible creatures being unintelligible.\n\nWhat is strange however, is one particular mooer.Lounging in the middle of the group, they are embracing the other mooers.Groping and fondling each and every one of them.But they are speaking(Screaming) out in normal words, and not just mooing.\n\nYou watch their shenanigans intently for an extended period of time, and your suspicions are quickly confirmed.The way they walk, talk and act are definitely human, even more obvious with the herds of Mooers surrounding them.Eventually the strange mooer leaves.Heading straight for you!You hide and just as they get near, they turn just at the last second and exit the park, entering an open apartment complex just beside the entrance and disappear inside.\n\nTime passes and they don’t leave, and as far as you can tell there is not any other entrance or exit to this apartment(Other than the fire escape of course) that could hide from your view.\n\nPerhaps they are worth an investigation ? Though, who knows what you will find inside…`,
			// },
			//
			// rewards: [DestinationUnlock.APARTMENT],
			//
			// options: 'none',
			// },
			{
				title: 'Fast Food Plaza',
				content: {
					DEFAULT: `The park is surprisingly dangerous. The amount of roaming mooers, hidden by trees, water features or small mounds are numerous. But what you find is a bounty worthy of the effort. HOT DOGS. Along with numerous other fast foods are lined up in a plaza. The old owners of these stands are long gone, and they left their stand running! All the meat, bread and everything is still fresh and refrigerated.
					
					But as you come to claim your bounty.A lone Milker clambers down the path.Her eyes locking on you as she licks her lips.Shakes her breasts and comes in to attack!`,
					SUCCESS: `You successfully fend off the Milker. Giving you enough time to stuff your bags full of food. This should keep you supplied with food for a while. And if you ever come back here. Plenty more food to come.`,
				},

				options: [
					{
						title: 'Fight!',
						click: ({ store, grabText }) => {
							store.setEncounter(
								store.createFightEncounter('milker', grabText('sucess'), [
									{
										reward: 'food',
										amount: 4,
									},
								])
							)
						},
					},
				],
			},
		],
		MUNDANE: [
			{
				title: 'Return to the Strands',
				content: {
					DEFAULT: `You head back to the stands located at the park, you got a good amount of supplies last time you were here. And there still seemed to be plenty more to get. However. There are a lot of changed in your path. You will need to sneak around them. Or risk wasting an entire day as alerting one would alert tens if not hundreds.`,
					FAIL: `You try your best to avoid the roaming Changed wandering around the park. You are still unsure just why they enjoy this place so much! Perhaps bovine instincts leading them to a place with lots of grass? As you contemplate this thought you bumble head first into a changed who immediately points at you and moo’s loudly alerting all the nearby mooers and milkers to your position. You need to run and hide NOW or you will be overrun in seconds. You’ll just have to try again another day.`,
					SUCCESS: `Using your wits, agility or just good old fashioned human determination. You avoid the roaming Changed. The stands are as bountiful as last time, although you took most of the really valuable food sources last time, while tasty, the food stuffs this time will not last anywhere near as long. But should keep you stable for a while.`,
				},

				difficulty: 12,
				rewards: [
					{
						reward: 'experience',
						amount: 2,
					},
					{
						reward: 'food',
						amount: 2,
					},
				],

				options: [
					{
						title: 'Sneak Past',
						type: 'agility',
						success: undefined,
						fail: undefined,
					},
					{
						title: 'Watch their patrols',
						type: 'perception',
						success: undefined,
						fail: undefined,
					},
				],
			},
		],
	},
	Hospital: {
		UNIQUE: [
			{
				title: 'Hospital - Floor 1',
				content: {
					DEFAULT: `As you make your way through the entrance you cannot help but gawk at the destruction that has rained down upon the hospital. Most likely a hotbed for the changed when the apocalypse happened. Chairs are broken and scattered throughout the entry room. The reception table covered in dried milk, papers soaked in the changeds juices reek of the sweet milk. Clothing scattered all over the place. Located on and in the most bizarre of places, some under the chairs, others on the reception counter (Health and safety would be furious) but most annoying for you. the stairs have been blocked by not only falling debris, the chairs and the gurneys, but by sleeping Changed! Whatever your plan is, you will need to do it carefully. There are changed all over, both asleep and awake ones, the obvious way up is blocked by a wall of obstacles and the 'safer' looking way is filled with changed, most likely ex patients of the hospital. But no doubt somewhere in this hospital is a working elevator(Hospitals tend to have backup power for months in case of emergencies) or an unblocked set of stairs. You just need to find it first.`,
					FAIL: `Putting your plan in action you set off into the hospital. You have a few close encounters with the Changed. Ones you either missed or that came out of doors unsuspectedly. But you manage to wander through well enough. Then you find it! An elevator and next to it, a set of stairs! As you walk towards the elevator a loud moo echoes from the stairway. Waddling down the stairs a Mooer takes the final step out in front of you. She moo's once again. If the other Changed hadn't heard the first moo they definitely heard that one. At this rate she will wake up the entire hospital. You better deal with her before you get completely surrounded and get past her to that elevator or up those stairs.`,
					SUCCESS: `Putting your plan in action you set off into the hospital. You have a few close encounters with the Changed. Ones you either missed or that came out of doors unsuspectedly. But you manage to wander through well enough. Then you find it! An elevator, unfortunately it says 'Out of Order' on it. Next to it is a set of stairs that look like they might take you up to the roof. But is it worth a risk?`,
				},

				difficulty: 16,
				// NOTE: we get rewarded with 20 experience, but this isn't used as this encounter is a "win or die" encounter.

				options: [
					{
						title: 'Find a way to lure the Changed away',
						type: 'intelligence',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'Continue', //title: 'Fight!'
										click: () => store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![1], true),
									},
								])
							)
						},
						fail: ({ store, grabText }) => {
							store.createSubEncounterFight(grabText('fail'), (option) => {
								store.setEncounter(
									store.createFightEncounter(undefined, undefined, undefined, () => {
										// We could have a dialog text here, but for now we'll just swap to the second encounter.
										store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![1], true)
									}),
									true
								)
							})
						},
					},
					{
						title: 'Sneak past the Changed',
						type: 'agility',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'Continue', //title: 'Fight!'
										click: () => store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![1], true),
									},
								])
							)
						},
						fail: ({ store, grabText }) => {
							store.createSubEncounterFight(grabText('fail'), (option) => {
								store.setEncounter(
									store.createFightEncounter(undefined, undefined, undefined, () => {
										store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![1], true)
									}),
									true
								)
							})
						},
					},
					{
						title: 'Watch the Changed patterns and wait for an opening',
						type: 'perception',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'Continue', //title: 'Fight!'
										click: () => store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![0], true),
									},
								])
							)
						},
						fail: ({ store, grabText }) => {
							store.createSubEncounterFight(grabText('fail'), (option) => {
								store.setEncounter(
									store.createFightEncounter(undefined, undefined, undefined, () => {
										store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![0], true)
									}),
									true
								)
							})
						},
					},
				],
			},
			{
				title: 'Hospital - Floor 2',
				content: {
					DEFAULT: `You have made it to an elevator and a flight of stairs. Now you must decide what your best option is. Take the stairs up who know how many stories or try to get the elevator to work, if it even can.`,
					FAIL: `You manage to make it all the way up to the entrance of the roof. As you barrel towards the door, you are forced to stop in place. The hallway leading towards the roof is blocked by a singular Mooer, staring straight at you. You cannot help but feel as if this was planned. Why one here of all places, why in such an inconvenient place for you? It doesn't matter. You must get past her to get to the roof. It's time for (hopefully) your last fight.`,
					SUCCESS: `You made it! You manage to get to the roof, though not before the Changed manage to finally track you down. Just as you reach the roof you can hear them coming...`,
				},

				difficulty: 16,
				// NOTE: same as previous encounter

				options: [
					{
						title: 'Fix the Elevator',
						type: 'intelligence',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'To the roof!', //title: 'Fight!'
										click: ({ gStore }) => {
											const playerStore = usePlayerStore()
											gStore.setGameOver(playerStore.escapeEnding)
										},
									},
								])
							)
						},
						fail: ({ store, grabText }) => {
							store.createSubEncounterFight(grabText('fail'), () => {
								store.setEncounter(
									store.createFightEncounter(undefined, undefined, undefined, ({ gStore }) => {
										const playerStore = usePlayerStore()
										gStore.setGameOver(playerStore.escapeEnding)
									})
								)
							})
						},
					},
					{
						title: 'Power your way up the stairs',
						type: 'stamina',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'To the roof!', //title: 'Fight!'
										click: ({ gStore }) => {
											const playerStore = usePlayerStore()
											gStore.setGameOver(playerStore.escapeEnding)
										},
									},
								])
							)
						},
						fail: ({ store, grabText }) => {
							store.createSubEncounterFight(grabText('fail'), () => {
								store.setEncounter(
									store.createFightEncounter(undefined, undefined, undefined, ({ gStore }) => {
										const playerStore = usePlayerStore()
										gStore.setGameOver(playerStore.escapeEnding)
									})
								)
							})
						},
					},
				],
			},
		],
		MUNDANE: [
			{
				title: 'A Chance Encounter',
				content: {
					DEFAULT: `The radio crackles to life as you finish plugging everything in. Time passes as you try calling into the void. Using different frequencies, bands, volumes, and other settings.
					
					"H-hello? Who is there?" a crackly voice comes through the speaker, just barely legible through all the static. You spare once again into the microphone, but he doesn't seem to respond.
					
					"You still there? If you are, I'll be at EverBrook Community Hospital in 3 horus on the roof with a helicopter. Hurry!" He repeats this over and over. EverBrook Community Hospital is only an hour walk from your location. You best get going if you want to be on that helicopter.`,
				},

				options: [
					{
						title: 'To the Hospital!',
						click: ({ store }) => store.setEncounter(ENCOUNTERS.Hospital.UNIQUE![0]),
					},
				],
			},
		],
	},
	Office: {
		UNIQUE: [
			{
				title: `The Lab`,
				content: {
					DEFAULT: `You step through the lab door to the sight of the strange mooer or perhaps better to call her by the name she gave you, Lindara from before. She is standing side by side with the one you saw in the dress looking like she was commanding the Mooers. She smiles at you before the Lindara speaks.
					"Oh good, you made it. I was worried you had become a mindless Mooer in the time we last met.As you can see. This place is rather unique..." She lets out a loud giggle as you look around.
					The walls are lined with monitors showing cameras around the city.Mooers filling most of them but one stands out. A hotel lobby, doors barricaded with furniture, several humans are gathered speaking to each other.You cannot quite tell what they are saying, but they seem calm enough.
					"Humanity, selfish, pointless, a pox on the world in my mind. Elongating and shortening the cycles of the world that instilled life for millennia.A statistical anomaly that we abuse and neglect.For hundreds of years I researched ways to improve humanity and it came one fateful day with milk.Milk, a substance harvested from cattle, perhaps the most harvested good from a sentient creature.The perfect analogy of a world we exploit.That and I do quite enjoy the taste of milk." 
					Lindara claps her hands as the door behind you slams shut, out from the shadows several Mooers surround you, these ones however are all wearing clothing (albeit rather poorly on their bovine bodies) and seem rather cognisant, perhaps because they are not immediately trying to force feed you milk.
					"You did well getting here.I was honestly surprised when I first saw you.We had had rumours of a human running around the outskirts of the city. But I did not expect to see one so close to the centre.So I positioned myself where we met and waited for you to pass by.You saw through my words, not that I was trying to be too secretive, any idiot could see I was lying.So you are not an idiot.And the way you survived in this city is quite impressive. So as a reward I will give you a choice.Join me as one of my private advisors and help rid this world of human menace.Or be forever doomed as a test subject for our new inventions to be tested upon.The choice is simple, no?"`,
				},

				reachable: false,

				options: [
					{
						title: 'Accept',
						click: ({ gStore }) => gStore.setGameOver('DEAL'),
					},
					{
						title: 'Refuse',
						click: ({ gStore }) => gStore.setGameOver('REFUSE'),
					},
				],
			},
		],
		MUNDANE: [
			{
				title: `A 'Normal' Office`,
				content: {
					DEFAULT: `You finally find the building marked out on the map the rather peculiar mooer. As you walk into the entrance it does not take long to notice it is barren. Very barren. If this was used as an office they believed in an insanely minimalist layout plan. There must be something here though. Why would she send you here if not?`,
					FAIL: `You search for hours upon hours, yet you find nothing more than empty rooms and more empty rooms.You do find a suspiciously clean spot in the basement, but no obvious way of opening it.The sky grows darker, and if you don’t leave now you will not get back to your safe spot in time.You’ll just have to come back tomorrow.If you can afford to potentially waste the time.`,
					SUCCESS: `You figured it out. It took a while, but you found it. In the basement a part of the wall was suspiciously cleaner than the rest. With not even a speck of dust on it’s frame or near the floor around it. While the rest was not covered in dust, this stood out when closely examined. But no obvious mechanism was found near the disturbed area. Scouring the building up and down you found a small button hidden on the bottom of one of the drawers (subsequently scratched by what looked like hooves) and when you returned back the door was just closing. Your surroundings turn sterile and pristine, chrome walls lead down like a hallway towards an electronic door with the word “Lab” printed on the front.`,
				},

				difficulty: 16,

				options: [
					{
						title: 'Figure this out',
						type: 'intelligence',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'Continue',
										click: () => store.setEncounter(ENCOUNTERS.Office.UNIQUE![0], true),
									},
								])
							)
						},
						fail: undefined,
					},
					{
						title: 'Use your keen senses',
						type: 'perception',
						success: ({ store, grabText }) => {
							store.setEncounter(
								store.createSubEncounterSimple(grabText('success'), [
									{
										title: 'Continue',
										click: () => store.setEncounter(ENCOUNTERS.Office.UNIQUE![0], true),
									},
								])
							)
						},
						fail: undefined,
					},
				],
			},
		],
	},
}
