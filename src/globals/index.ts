import dedent from 'dedent'
import { TEXT, TEXT_TRANSLATION } from '../globals/text'
import { useGlobalStore } from '../store/global'
import { usePlayerStore } from '../store/player'

export const GAMEMODE = ['cowgirl', 'hucow'] as const
export type TGameMode = typeof GAMEMODE[number]

export enum KINKS {
	Udders = 1,
	Herms = 2,
}

export type GameScreens =
	| 'MainMenu'
	| 'Introduction'
	| 'CharacterCreation'
	| 'GameScreen'
	| 'GameOver'
	| 'GalleryScreen'

export function $traverseText(tbl: any, keys: string = ''): string {
	let data = tbl

	if (typeof data === 'object') {
		for (const KEY of keys.toUpperCase().split('.')) {
			if (!data.hasOwnProperty(KEY)) {
				console.error(`Cannot find index ${KEY} of ${keys}`)
				console.log({
					tbl,
					data,
				})
				return '<ERROR>'
			}
			data = data[KEY]
		}
	}

	if (typeof data === 'object') {
		const globalStore = useGlobalStore()
		const playerStore = usePlayerStore()

		let buffer = data
		while (true) {
			if (data.hasOwnProperty('FURRY') && data.hasOwnProperty('HUCOW')) {
				data = data[globalStore.gameMode === 'cowgirl' ? 'FURRY' : 'HUCOW']
			}
			if (data.hasOwnProperty('MALE') && data.hasOwnProperty('FEMALE')) {
				data = data[playerStore.isPlayerMale ? 'MALE' : 'FEMALE']
			}

			if (typeof data === 'string') break
			if (JSON.stringify(buffer) === JSON.stringify(data)) {
				console.error('Cannot progress further: ' + keys)
				console.error(data)
				break
			}
			buffer = data
		}
	}

	return dedent(data)
}

export function $grabGlobalText(key: string): string {
	let text = $traverseText(TEXT, key)

	if (text.startsWith('$redirect') && text.endsWith('$')) {
		const redir = /\$redirect\.(.+)\$/g.exec(text)!
		return $grabGlobalText(redir[1])
	}
	text = $translateText(text)

	return text
}

export function $translateText(text: string) {
	const globalStore = useGlobalStore()

	if (/\$translation:([^\$]+)\$/g.test(text)) {
		for (const match of text.matchAll(/\$translation:([^\$]+)\$/g)) {
			if (match[1].split('|').length === 2) {
				const split = match[1].split('|')
				text = text.replace(match[0], globalStore.gameMode === 'cowgirl' ? split[0] : split[1])
			} else {
				const translation = $traverseText(TEXT_TRANSLATION, match[1])
				if (translation === undefined) throw 'Could not find translation: ' + match[1]
				text = text.replace(match[0], translation)
			}
		}
	}

	return text
}

export function $capitalize(text: string) {
	return text.substring(0, 1).toUpperCase() + text.substring(1)
}

export function $grabEventInput(event: Event) {
	return (event.target as HTMLInputElement).value
}
