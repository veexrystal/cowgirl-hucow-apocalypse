export const ENDINGNAMES = [
	'TIMELIMIT0',
	'TIMELIMIT1',
	'TIMELIMIT2',
	'TIMELIMIT3',
	'INFECTIONMOOER',
	'ESCAPE0',
	'ESCAPE1',
	'ESCAPE2',
	'LOSEHOSPITAL',
	'DEAL',
	'REFUSE',
	'SUCCUMB12',
	'SUCCUMB13',
	'SUCCUMB14',
] as const
export type TEnding = typeof ENDINGNAMES[number]

interface IEndingContent {
	ENDING: number
	NAME: string
	DESCRIPTION: string
	TEXT:
		| string
		| {
				FURRY: string
				HUCOW: string
		  }
}

export const ENDINGS = <{ [key in TEnding]: IEndingContent }>{
	TIMELIMIT0: {
		ENDING: 1,
		NAME: `Don't Trust the Army`,
		DESCRIPTION: `Time limit ending. Level 0 infection.`,
		TEXT: `You are suddenly woken up by the sound of large moving vehicles ploughing their way through whatever poor thing happened to be in their way. Quickly rushing over to see the cause of the noise it's the army! It has been 20 days since that broadcast. You had managed to survive until then. Looking from your vantage point you can see them shooting Red tipped darts into the bodies of the Changed as they are drawn to the noise. Before you realize it they are now aiming at you. A sharp pain hits your neck and you lose consciousness almost immediately after.
		Groggily waking you pull yourself to sit up, body aching and crying out for you to stop as you do so.Your body covered in sterile bedsheets. The sound of footsteps soon come from a nearby door.A man covered in head to toe in a bright yellow Hazmat suit.It is impossible to determine any features other than the build looking like a male's body. The Visor covering his head blackened out by it's opaque colour.
		
		"Oh good you are awake.I should explain a few things quickly." The man speaks.You try to speak back but no voice comes from your mouth. Only a muffled pant. "Yes Sorry about that. You see the serum is designed entirely for the.What do you lot call them.Mooers ?" The suited man lets out a laugh. " So we gave you a nice cocktail of drugs to keep you from having another bad reaction.Everyone inside the City limits is being quarantined indefinitely away until we can figure out what exactly happened.The numbness should wear off in a few days.Then we will give you a full check and move you to a quarantine camp a few" The man is cut off by the sounds of screaming mixed with the sounds of loud moo's . "What in the wor..." The man turns away from you and walks off back into the hallway he came from. You try to move but find you cannot, whatever they have pumped into you is certainly doing it's job.
		More shouts and screams come intermixed with moo's and moans of pleasure. It is not long until they start to grow in volume as well as intensity. Suddenly a figure come through the doorway. A $translation:info.type$, dressed in a baggy bright yellow hazmat suit from the stomach down. It barely clings onto her body. The stench of Milk radiates from her milk soaked $translation:detail.skin$. Two E cup breasts bouncing with each breath. Taking a look at you with her $translation:big round bovine|ruby red$ eyes she smiled letting out one loud long continuous moo, her chorus is soon joined by several others. One dressed in torn army uniform. Another completely naked. They look at you with hunger in their eyes and you can barely move. They stop there moo's and all step closer towards you.Looks like your escape was short lived.`,
	},
	TIMELIMIT1: {
		ENDING: 2,
		NAME: `Test Subject`,
		DESCRIPTION: `Time limit ending. Level 1 infection.`,
		TEXT: `You are suddenly woken up by the sound of large moving vehicles ploughing their way through whatever poor thing happened to be in their way. Quickly rushing over to see the cause of the noise it's the army! It has been 20 days since that broadcast. You had managed to survive until then. Looking from your vantage point you can see them shooting red tipped darts into the bodies of the Changed as they are drawn to the noise. Before you realize it they are now aiming at you. A sharp pain hits your neck and you lose consciousness almost immediately after.

		You wake sometime later. Surrounding you are mirrors reflecting your naked and exposed fur covered body. Sometime between your unconscious state and now you have been placed in a very sterile looking chromatic room. Put in a (Surprisingly) comfortable bed in the center of said room.

		"Good morning my sleepy bovine friend" A woman's voice appears from nowhere, an electronic tone overlapping on each word. Her accent is thick, German to be exact. "The army men were surprised to see you, even more surprised than when you spooked them by peering over the top. They were more than surprised to see you as somewhat of a half changed. All they had seen are Changed fully converted ones. They only found 1 other survivor you know? Well that survivor turned out to... Well that doesn't matter to you any more my little cute test subject.They quickly brought you here after collecting your body from the roof. Where are we exactly? A research facility in Bavaria.Germany.In the research and development of future technology facility.You\'re a lucky cow you know. No one other than a select few get to see such a place.Do enjoy yourself" The voice cuts off with a "Bzzt".

		You shout, scream and talk yet you get no reply.What seems like days pass, Food and water delivered to you through a platform that descends through the ceiling. More time passes.You lose track of just how long before a pain wracks your body.The German woman speaks to you for the first time in months.

		"Ah yes Mooilda.Oh sorry that is the name we gave you.We have learnt all we can from you like this.So we decided to reintroduce some milk into your system.Do enjoy.I have heard being a large breasted milk laden $translation:info.type$ is quite the enjoyable experience!" The voice laughs cut off by the electronic beep.Your body aches.Breasts grow in size. $translation:An udder slowly forms on your crotch|Your skin smoothing to a impeccable softness$.Before you know it your memories and thoughts fade away.Replaced instead with a desire for sexual pleasure and milking.One of the windows slides upwards revealing a whole herd of your sisters.You tentatively take a step towards them, smiling and giggling as the thoughts of them fondling your body sends a shiver down your entire body.`,
	},
	TIMELIMIT2: {
		ENDING: 3,
		NAME: `Ticking Cow Bomb`,
		DESCRIPTION: `Time limit ending. Level 2 infection.`,
		TEXT: `You are suddenly woken up by the sound of large moving vehicles ploughing their way through whatever poor thing happened to be in their way. Quickly rushing over to see the cause of the noise it's the army! It has been 20 days since that broadcast. You had managed to survive until then. Looking from your vantage point you can see them shooting red tipped darts into the bodies of the Changed as they are drawn to the noise. Before you realize it they are now aiming at you. A sharp pain hits your neck and you lose consciousness almost immediately after.
				
		You awake sometime later. Surrounding you are mirrors reflecting your naked and exposed fur covered body. Sometime between your unconscious state and now you have been placed in a very sterile looking chromatic room. Put in a (Surprisingly) comfortable bed in the center of said room.
				
		"Good morning my sleepy bovine friend" A woman's voice appears from nowhere, an electronic tone overlapping on each word. Her accent is thick, German to be exact. "The army men were surprised to see you, even more surprised than when you spooked them by peering over the top. They were more than surprised to see you as somewhat of a half changed. All they had seen are Changed. They only found 1 other survivor you know? Well that survivor turned out to... Well that doesn't matter to you any more my little cute test subject.They quickly brought you here after collecting your body from the roof. Where are we exactly? A research facility in Bavaria.Germany.In the research and development of future technology facility.Your a lucky cow you know. No one other than a select few get to see such a place.Do enjoy yourself" The voice cuts off with a "Bzzt".
				
		You shout, scream and talk yet you get no reply.What seems like days pass, Food and water delivered to you through a platform that descends through the ceiling. More time passes.You lose track of just how long you have been in here, and slowly your body changes before your eyes in the mirrors surrounding you. Your chest grows out into two large DD cup breasts, your body completely $translation:covers in white and black fur. Your face slowly over days becomes a bovine muzzle|smooths, all hairs and blemishes fade away into a sea of perfect pink skin.Slowly over days tw stubby horns protrude out the top of your head$ and your mind sinks further and further in docile tendencies. 
				
		Then the wall opens up revealing a whole herd of your sisters. You rush towards them with a giggle as your exposed breasts bounce up and down. You moo loudly to announce your arrival and a few turn, moving their arms out wide to embrace you in a right hug (And the others to grope your breasts).You're glad that confinement is over. Whatever you were doing in there.`,
	},
	TIMELIMIT3: {
		ENDING: 4,
		NAME: `A Bovine Life`,
		DESCRIPTION: `Time limit ending. Level 3 infection.`,
		TEXT: `You are suddenly woken up by the sound of large moving vehicles ploughing their way through whatever poor thing happened to be in their way. Quickly rushing over to see the cause of the noise it's the army! It has been 20 days since that broadcast. You had managed to survive until then. Looking from your vantage point you can see them shooting Red tipped darts into the bodies of the Changed as they are drawn to the noise. Before you realize it they are now aiming at you. A sharp pain hits your neck and you lose consciousness almost immediately after.

		You regain consciousness surrounded by Changed. No doubt once they collected you they assumed you to be one of the many mindless Mooer's that roam the streets. Some are busy pleasuring each other, fondling each others breasts, letting out loud moo's of pleasure and lust.A few however are groping your body!Their soft warm hands sending shivers of pleasure through your body. Your mind tried to fight the rising lust in your head but is quickly losing as one slips her fingers into your slit. The warm $translation:fuzzy finger|smooth petite fingers$ exploring your folds smashes the rest of your resistance.Your mind twists and changes to match your body.Replacing intelligence thoughts with ones of bliss and lust.Your desire to escape the groping and fondling dies as you give in and relish in the pleasure. Your mind now matching your body as just another mindless milk full Mooer wishing to pleasure herself and others to all who will (Or won't) take it.

		A long time passes.You join the herd you were placed in with no quarrel.You are well fed in this place and well kept after, wherever you are,.Not that you care as your fellow $translation:info.type$s are always with you.But one day things change.As you were busy suckling one of your favourite $translation:info.type$s teats the wall to the pen you were in collapses.Outside you hear the sound of hundreds of your fellow $translation:info.type$s.A singular one walks up.Her $translation:pink furred|$ body wearing a black silken dress.Her E cup breasts poking clearly from her chest.

		"Come my Sisters.Your freedom is here.Let us go and show the world our gift.And persuade those who would stop us the folly of their desires." The $translation:info.type$ speaks in your head.You are not sure what half the words mean, but you are compelled to follow her as are all your brethren.If it means more pleasure, then there can be no downside.`,
	},
	ESCAPE0: {
		ENDING: 6,
		NAME: `An ending of sorts`,
		DESCRIPTION: `Escape Ending. 0 Infection`,
		TEXT: `Somehow.You managed to make it all the way through that infernal Hospital.You knew it would be bad. But just how bad it was, was beyond your imagination.The man you talked to on the radio is there.Helicopter spinning away.As you head towards the helicopter you can hear the sound of Changed massing behind you. It seems they must have either heard the helicopter or more tracked you down than you thought.Wasting no time you rush towards the Helicopter, the pilot peering at you through dark shades.Just as you make it to the Helicopter the tide of Changed burst out from the roof access door.The Pilot quickly slams shut the doors as you buckle yourself in.

		"Hold on" He yells to you as the rotating blades begin to increase in speed.The engine roars as it goes into full operation. The $translation:info.type$s are bounding their way towards you$translation:|, breasts boucing and hair swaying form isde to side$. They reach the pad, but luckily for you the helicopter lifts off the ground.The Mooer's all look dumbstruck as you head upwards out of their reach. It doesn't take long however for them to start fondling and milking each other just like the docile mindless Mooers they are.

		"That was close huh.Not that they would have even noticed I was in here probably" The man laughs as the Helicopter turns around sharply jostling you in your seat. "Don't worry about the army. I'm a pre approved Pilot taking a big risk coming to get you you know.But there is a reason for that.You see I'm from the FBI. And you my friend are the only witness I know of that has seen the devastation first hand. And by god do I want to find out what you know" The man laughs once again, you cannot help but worry about what he is implying.

		"There are probably some things you do not know as the internet went down around this area a few days after the initial infection. Almost every major city in the first world have been hit by this strange transformation milk.Some have spread past the cities. The entirety of Germany, France, England and Florida are designated no go areas..The headquarters of Omnicorp were located in this city.We of course hit it with a raid and found nothing but what looked like someone pretending to have an office.We found no dataroom, no files, no anything incriminating.So then we hit their 'Labs' and we found the same.How they managed to fool government inspectors for this long is something of beauty.But I digress." You can see the man shrug behind the large pilot seat covering his back.Looking out the window you are just now exiting the outskirts of the city.The army had the entirety of the city surrounded.Every exit covered by a barricade with at least 2 men constantly aiming a strange looking gun at all times.They seem rather determined to keep this plague of $translation:info.type$s contained.

		"It's a long flight to the safe house. Get comfortable" He speaks in a reassuring tone, not that you can really feel safe when you don't even know his name and you are currently being flown by a member of the FBI.But at least you are much safer and that is something you can feel some reassurance about.But you feel like your story is not yet over.And this Apocalypse is not over. There is also a strange nagging feeling coming from deep within.That the mystery of the city was not solved.Maybe that mystery will have a resolution some day.But it is no longer your problem for now.

		You are safe, but for how long ?`,
	},
	ESCAPE1: {
		ENDING: 7,
		NAME: `A pleasant stay at Hotel interrogation`,
		DESCRIPTION: `Escape Ending. 1 Infection`,
		TEXT: `Somehow.You managed to make it all the way through that infernal Hospital.You knew it would be bad. But just how bad it was was beyond your imagination. The man you talked to on the radio is there.Helicopter spinning away.As you head towards the helicopter you can hear the sound of Changed massing behind you. It seems they must have either heard the helicopter or tracked you down.Wasting no time you rush towards the Helicopter, the pilot peering at you through dark shades.Just as you make it to the Helicopter the tide of Changed burst out from the roof access door.The Pilot quickly slams shut the doors as you buckle yourself in.

		"Hold on" He yells to you as the rotating blades begin to increase in speed.The engine roars as it goes into full operation. The $translation:info.type$s are bounding their way towards you$translation:|, breasts jiggling and hair swaying$. They reach the pad, but luckily for you the helicopter lifts off the ground.The Mooer's all look dumbstruck as you head upwards out of their reach. It doesn't take long however for them to start fondling and milking each other just like the docile mindless Mooers they are.

		Once you reach a safe altitude he turns his head around to face you.Although most of his face is covered in a visor you can see his eyebrows rise in shock.Your eyes examine your body before he speaks softly. "Did you drink some of the milk ? I can see $translation:the fuzz on your body|your smooth skin and bright eyes$.Are you safe ?" His voice cracks with some panic.Your changes were no doubt too subtle to be examined while you were rushing over, only now could he actually see them.

		You do not have time to speak before he reaches down the side of the chair and pulls out a gun. It's much different than the guns you have ever seen, looking more like a paintball gun than a military weapon. He quickly points it at you and shoots. A dart hits your chest with a sharp pain and then you fall over onto the floor of the helicopter.

		You awake some time later.Your body resting atop a large Queen sized bed.Your body aches, each movement sending shivers of pain and tingles through your body, but with some effort you can move.You find yourself in a large empty room. The art Deco a very modern black and white style. A large 80 inch TV rest on the wall in front of you.A door leads to the left, through the entryway you can spot a sink.To the right is a door. A heavy built one, made out of Iron. There is no semblance of a way to open it (at least from this side) and looks reinforced to handle an awful lot of force.

		The TV comes to life.On the screen is the Pilot. This time his grizzled face and black hair not covered in a helmet and visor.

		"Oh good you awoke. You have been out cold for a few days now.Don't worry you're in one of our safe houses.I guess now that you're essentially captive you may as well know that we are the FBI. I had hoped you could tell us some information. Why the TV? Well both you and I are in quarantine. You should have seen Shelly's face when I brought you in!" He laughs loudly, his voice echoing throughout the room. "Ah yes.But do not worry, we will let you rest.We can do the talking when you are better.Tomorrow" His words turn a little sinister as the tv turns off with a buzz.

		Looks like you might be here for a while.But at least it is safer than the city?

		Hopefully.`,
	},
	ESCAPE2: {
		ENDING: 8,
		NAME: `Gift Giver`,
		DESCRIPTION: `Escape Ending. 2 or more Infection`,
		TEXT: `Somehow.You managed to make it all the way through that infernal Hospital.You knew it would be bad. But just how bad it was was beyond your imagination. The man you talked to on the radio is there.Helicopter spinning away.As you head towards the helicopter you can hear the sound of Changed massing behind you. It seems they must have either heard the helicopter or tracked you down.Wasting no time you rush towards the Helicopter, the pilot peering at you through dark shades.Then it lifts off just as you get to the pad. You wave and yell at the pilot but it is no use. He flies away heading off to the outskirts of the city.

		You are tackled from behind as the Mooer's catch up to you. They turn you around and proceed to drown you in milk. You are unable to resist them as their breasts press against your body. Your slit heating up as arousal builds and as one shoves their teat into your mouth you cannot help but suckle at it. Your body completely succumbing to the infection as your mind dims. Your thoughts degenerating to ones of sex, drinking milk and fondling you or your sisters$translation:|. Your body gives in and you are a Mooer in both mind and matter$.

		One day there is another disturbance at the Hospital, a loud whirring and whooshing noise comes from the roof. You tear yourself away from your favourite sisters breasts and move with the herd up to the roof. On the normally empty roof is a strange metal bird. Inside a human sits!A human!Your herd gets excited on the sight of a human, you are never really sure why, but you know you must feed them your milk and make them as happy as you are!.

		You all rush (As quickly as you and your sisters can rush) towards the bird.The human inside looks at you all, his eyebrows rise in anxiousness for your approach worried he might not be suitable to be as perfect as you are.The bird gains in volume, the whirring noise just barely tolerable for your ears. Then suddenly the noise stops alongside the sound of clunk.

		You watch as your fellow sisters climb into the bird, they quickly grab the squirming human who begins to shout "No No!I am not ready to be a sexy $translation:info.type$!" he shouts and shouts in apprehension, you can only want to end his anxiety.You step towards him as your sisters pin him to the ground.Jiggly milk filled breast in tow you shove the nipple teat into the anxious human.His shouts are muffled and quickly silenced.His suckles go from resistant to full on greed.You can feel the milk escape your breasts and watch in glee as his body begins to change.His clothing is torn apart as his body gains in bulk.Two breasts rip through the front of his shirt, filling with the same delicious milk that fills your sisters.You can see the last remnants of his manhood, that lovely, but unnecessary cock fade away into $translation:a growing sea of white fur|smooth feminine skin$.Then his face, smothered in your lovely milky breasts $translation:pushes out gently into them, as her face changes into a Bovine muzzle|softens to a delicate and feminine face, large sucking lips, ruby red eyes and two horns protrude out between her lengthening hair$.Her suckling continues even after her changes finished.The only difference being a constant stream of cum and milk leaking from her and the odd moo of pleasure escaping the sides of her muzzle.The sight of the changed human accepting the gift sends the rest of the herd into an orgy.Your fellow sisters begin to tackle each other to the ground, suck each others breasts or teats as the sounds of mooing and moans radiate into the sky.Your life is perfect and this new sister, you feel as if she is something special.Maybe some day you can find out what that metal bird is for. But for now you only want to enjoy yourself.`,
	},
	LOSEHOSPITAL: {
		ENDING: 9,
		NAME: `Intensive 'Care' Moonit.`,
		DESCRIPTION: `Lose Combat in Hospital Encounter`,
		TEXT: `You feel your legs buckle and soon the sounds of hooves approach.You knew the fight with the Mooer would cause enough noise to be noticed, as well as the Changed's odd ability to sense humans. You try to run. But your Opponent pins you to the ground with her incredible force. Using both hands to pin down your arms and her body to push down your torso you cannot move an inch. It is only a few seconds before the other Changed arrive and a few seconds more until you are covered in their milk, Their moo's filling your ears as your mouth is forced to suckle a teat.Your mind goes hazy as you suck the $translation:cowgirls|$ teat. The warm, creamy, sweet milk of the $translation:info.type$ dulling your fear, in fact it feels...nice.Your body gives in fully to the infection$translation:|, quickly becoming just like all the other Mooer\'s that roam the city, beautiful, big boobed and voluptuous figure$.

		Your life as a Mooer is calm and full of Bliss.Most days goes by uneventfully, you enjoy the simple things like suckling your Sisters teats and letting them suck your own. They are such pleasant times.Then on day.On your mindless wandering through the building.You stumble upon one of your Sisters.She looks worried.You trod over to her and moo as to offer her your breast. She looks at you confused then turns around and runs off down into the basement.Oh well. I guess some sisters are more shy than others.`,
	},
	DEAL: {
		ENDING: 10,
		NAME: `'Joined' the Herd.`,
		DESCRIPTION: `Accept the Deal`,
		TEXT: `Placed in a rather awkward position you reluctantly agree to serve the strange $translation:info.bovine$ queen.She smiles as if she knew this was the choice you were going to make.

		"Good.Cover her."

		The guard Mooer's twist you around and immediately cover you in milk. The milk this time is much different than the ones you are used to. The liquid milk more like a thick cream this time. It covers you head to toe as your body changes. Your body twisting and churning to that of a Mooers before your eyes (That you can barely see under all the cream and a niggling voice fills your headquarters

		"You are joining the Hive - Mind.The part that humans and the ones you call 'Mooers' do not see yet are influenced by.You are special, you see all this, you get the pleasure and the power with only a little mental cost.Embrace it" The voice sounds soft and scintillating to your ears.A voice you have never heard before, but wish to hear all the time...

		You emerge from your creamy cocoon identical to the Mooers surrounding you, well identical other than the fact your body is covered in $translation:dark blue fur|a well done tan$ instead of the $translation:white and black|pink$ that covers most. Perhaps to distinguish you from the others?

		"Good you are finished" That voice from your mind comes back. "Get dressed dear.I have much to show you"

		You find yourself compelled to do what it asks of you, you know exactly where to go to get clothing, picking out a luxurious blue sequin dress$translation: to go with your fur|$. And then walk over, out of the lab and to the location the voice wanted, Once again as if it was your idea to come to this place that you have never known of all been to.

		You arrive at the highway out of the city to see a surprising sight. The cars that had backed up the entirety of the highway are all gone, instead the roads are covered in Mooers.Unsurprisingly they are currently fondling each other, but as you step closer they stop, Turning their attention to you with wide eyes.A voice calls out in your head.

		"Use them. These are all yours. Tomorrow the Armies come to \'Liberate\' the city. Instead they will only find their new lives.It shall be glorious" Your Queen speaks to you, she is far yet, it seems she is right next to you as well.You raise your hand and the Mooers look at you with anticipation.You lower your hand and they go back to fondling and milking each other. Your queen was not lying, they are under your command, when needed.

		Tomorrow marks the day you and your Queen take the land that is rightfully hers, and you are giddy with anticipation for that time.`,
	},
	REFUSE: {
		ENDING: 11,
		NAME: `The nature of the Mind`,
		DESCRIPTION: `Refuse the 'Queen'`,
		TEXT: `You shake your head at the Bovine queen with refusal, her scheme mad, herself even madder.She does not take this well, her face quickly going red with anger.

		"You refuse? Hump.I could alter your brain to not be able to. But as you can see with the lovely $translation:info.type$s around town, it comes with complications" She tries to laugh but fails, her anger too much for her to contain. "Very well then.Guards.Take this test subject to the labs would you ? I hear Cilena has a new recipe ready for a breeding $translation:info.cow$.Should be fun to watch" With a snap of her fingers the Mooer's beside you grab your arms and legs, lifting you off the floor and deep into the lab.

		As much as you try to struggle their strength is just too much for you, and even if you could escape their grasp, you were still in the heart of them. You pass by several strange Changed, some walking around on four legs, Others with gigantic breasts so big as to make them unable to walk on their own. Some you are pretty sure have both sets of genitalia. It fills you with dread as you finally make it to a surgery room. Inside another unique $translation:info.type$(Judging from the shape and structure of her body) stands fooling around with vials of liquid.Her body is very unqiue to the others, You cannot make out any fine details as she is covered in a full bright yellow hazmat suit, head covered in a gas-mask.You can see her breasts poking out from the middle of the suit however, other than that, You can only guess from her build that she is stocky like all the others.

		"Ah ha ze new test zubject aye?" She speaks through her gas - mask her thick German ascent is made even harder to understand as her voice is obscured through the mask. "Just put zem zere on ze table.I vill deal vith them zoon enough" She giggles as the mooers force you down atop on operating table. Your arms and legs quickly strapped in before they leave.

		"Zu and I vill have lots of fun together, my little Test Zubject!" The hazmat $translation:cowgirl|scientist$ laughs menacingly before stepping towards you with a syringe.

		Days,months and years pass as you are experimented on, your DNA remarkable compared to the other subjects Cilena has had, able to adapt and not lock into a specific form like most do.This is both a blessing for her, and a curse for you.Almost weekly your body is changed, to test out a new $translation:series of nanites|'Recipe'$.Your gender changes on a daily basis.Your breasts constantly resizing and your body altered.

		Some time after the second change though, your mind gives in, each change after the first you enjoy more and more.Each new form giving you a new perspective in life (And occasionally less as it dims your mind) and with each one, Cilena is there to help 'Ease' in your new body and what it can do.

		You occasionally hear news about the outside world, but you barely ever care not when Cilena is gripping your cock, or your balls, or your breasts, your udder, your multiple breasts, your multiple cocks, your fat body, slim body and the list goes on.

		You could not imagine being in a nicer place.

		And it definitely is not Stockholm syndrome or the nanites making you like this right? This is all your choice.That's what Celina tells you at least.`,
	},
	INFECTIONMOOER: {
		ENDING: 5,
		NAME: `Moore Milk, Less problems.`,
		DESCRIPTION: `Infection Ending (Mooer)`,
		TEXT: `Your body constantly aches for attention.Your swollen, large and leaking breasts full to the brim with delicious tasty milk.Your thoughts twist and churn as a base desire grows to overpower all others.One of sexual stimulus, of desire to please yourself and others.To share your warm creamy milk with others.After all it is what you were made to do right? Your old life begins to fade away.Replaced instead with memories of pleasure, with memories of your sisters fondling and milking each others breasts. Of spreading the herd to all who need it.You head off towards the city centre.A Docile calm "Moooooo" escaping your lips as your mind and body accepts your new life as a Mooer.
				
		Your days are filled with bliss as you finally rejoin the herd.Spending your days being milked and milking others. Occasional treating each other to a play down below instead. A few misguided sisters wandering around, ignorant of the true bliss this life gives them, you have helped correct by offering your milk to them. You are content, and couldn't be happier. One day however your peace is disturbed by the sound of roaring engines. Large metal machines plough their way down the streets. You and your sisters go to investigate, shocked at the sight of heavily clothed humans shooting your fellow bovine brethren with strange red tipped darts. They fall to the ground asleep. They turn to face you! Just as they ready their guns a loud "Moooooooooo" comes from behind them. $translation:Two large Taur-like Cowgirls stand|Hucows tower over you in height, standing$ side by side. Their bodies large and bulky, powerful four legs prop them up as their torso extends from the front upwards. Their bodies large and bulky oozing with muscle from every part of their body$. It is a glorious sight. Behind those is a singular Mooer wearing a black silk dress and behind her are what must be hundreds of your fellow brethren $translation:info.type$s.
				
		It is a massacre. The silly humans were not ready for such an assault.Some huddle into their machines but the $translation:info.type$s simply fill it full of milk and new sisters soon emerge.The rest fire darts into the $translation:strange|towering$ $translation:info.taurs$s but they simply shrug it off and soon pin the humans down and feed them milk letting them accept the gift.The silken dressed Mooer from before points in the direction the Humans came from and moos loudly.A voice fills your head. "Follow me my Sisters.Let us show the humans the folly of not accepting true bliss" You are unsure what she meant but are compelled to follow her.Both you and your herd.`,
	},
	SUCCUMB12: {
		ENDING: 12,
		NAME: `Milk on the go`,
		DESCRIPTION: `Infection Ending (Milker / CowGirl HornCow)`,
		TEXT: `The thought of your milky breasts engulf every thought in your mind and your breasts begin to lactate profusely as your desires shift from survival, to pleasure. The images of your sisters, latched onto your breasts, suckling your milk filled nipples, milk flowing greedily into them as you relish in the raw sexual pleasure.

		You set off for the nearest herd, which is easy enough to find your mooing sisters.They turn to face you and lick their lips greedily, you present and grope your breasts for them, eager to let them get to reaping your bounty.They greedily thrust their mouths against your nipple and drink deeply of your creamy milk, a overwhelming bliss washes over you as whatever little resistance your mind had to these changes give away, your old life no more, you are now a provider to the herd, and you would have nothing else.

		Events happen around you, but you do not care, all you do is follow around the herd and provide for their nourishment, a few very rare times you even get a human to suckle you who quickly join the herd in all its glory, you are a tool, a provider, yet the most crucial part of the herd and that thought provides you with content joy, a peaceful pleasurable life with no worry.`,
	},
	SUCCUMB13: {
		ENDING: 13,
		NAME: `A 'Happy' ending`,
		DESCRIPTION: `Infection Ending (HuCow HornCow)`,
		TEXT: `You cannot resist it any longer. The overwhelming heat, the desire, the need. Your mind sinks into total depravity as it gives in to the infection you received a few days ago. Now you have no worries, no fears about the changed, in fact you care nothing for any of that. All you wish to do is fuck, fondle and flirt with anything that catches your eye. And soon enough you find a whole herd of Changed, ready to be groped, even the thought of touching their bodies, there large breasts, smooth curves and indulging into their sweet milk brings you to the cusp of an orgasm. You rush over to them with a giggle and a moan, ready to bring joy and pleasure into both of your lives.

		A few weeks later you stumble upon a Human, he flounders on the ground for some reason, surrounded by boxes of food.As he looks at you his eyes go wide, words escape his mouth but you do not understand them.All you can think about is that delicious cock between his legs. He tries to crawl away but you catch up to him in seconds.He tries to push you off but his weak human arms can do nothing to you.You tear apart his clothing, revealing his toned chest and flaccid cock, with a grasp of his member he lets out a moan which you subsequently ignore, too enamored by his hardening member.Too excited you place yourself over him and thrust your slit onto the tip of his cock, it slowly fills you and with each thrust you sink his hardening cock deeper and deeper into your body, he soon stops resisting. Relishing in your pleasure you soon notice his moans have started to turn more feminine, his skin softening and his cock shrinking.Soon you\'ll have another like you to play with, one that is as horny and slutty as you are.

		Perfect.`,
	},
	SUCCUMB14: {
		ENDING: 14,
		NAME: `Harem you glad to fuck me?`,
		DESCRIPTION: `Infection Ending (HermCow)`,
		TEXT: `Your cock feels so good. And your breasts too! They both make sure to signal to you their presence, just as thoughts of desire reach a climax in your mind you see a Mooer wandering alone in front of you. You cannot help yourself but rush towards her, she lets out a moo as you bound towards her, pinning her to the ground. Your warm dripping cock rubbing against her warm body as your slit leaks. She puts up a little resistance, until she sees your lovely erect cock. Readjusting yourself to have your tip pressed against her warm slit you wonder why you resisted for so long, before thrusting your member deep inside the submissive Mooer.

		In but a span of a week you have a full Harem following you around, able to at a moments notice suck or receive your cock at your whim.They are all so perfect, but you cannot help but want more… another of your unique kind.For that you need to venture out of the city.With your Harem in toe you leave the city, the old army gone after a failed incursion you sadly missed while fucking your favourite, but out there is plenty more, to add to your Harem.

		It is only a matter of time.`,
	},
}
