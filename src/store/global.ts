import { defineStore } from 'pinia'
import { GAMEMODE, GameScreens, KINKS, TGameMode } from '../globals'
import { TEnding } from '../globals/endings'
import { usePlayerStore } from './player'

export const useGlobalStore = defineStore('global', {
	state: () => ({
		gameMode: <TGameMode>'cowgirl',
		kinks: KINKS.Udders | KINKS.Herms,

		currentScreen: <GameScreens>'MainMenu',
		_nextScreen: <GameScreens | undefined>undefined,

		cheatsActive: false,
		cheats: {
			strongMinded: false,
			noRescue: false,
			stopInfectionProgress: false,
			halfDifficulty: false,
		},

		inGame: false,
		gameOver: <null | TEnding>null,
		dayLimit: 20,

		showingSettings: false,
	}),

	getters: {
		gameModeFriendly: (state) => {
			return (mode: TGameMode) => {
				return mode === 'cowgirl' ? 'CowGirl' : 'HuCow'
			}
		},

		udderKink: (state) => !!(state.kinks & KINKS.Udders),
		hermKink: (state) => !!(state.kinks & KINKS.Herms),
	},

	actions: {
		resetGame() {
			this.kinks = KINKS.Udders | KINKS.Herms
			this.gameMode = 'cowgirl'
			this.__setWindowTitle()

			const playerStore = usePlayerStore()
			playerStore.$reset()
		},

		__debug(str: any) {
			console.log(str)
		},
		__setWindowTitle(str?: string) {
			document.title = str ?? `${this.gameModeFriendly(this.gameMode)} Apocalypse`
		},

		setMode(mode: TGameMode) {
			this.gameMode = mode
			this.__setWindowTitle()
		},
		toggleMode() {
			this.gameMode = this.gameMode === 'cowgirl' ? 'hucow' : 'cowgirl'
			this.__setWindowTitle()
		},

		setKink(flag: number) {
			this.kinks |= flag
		},
		toggleKink(flag: number) {
			this.kinks ^= flag
		},

		setGameOver(key: TEnding, nextScreen?: GameScreens) {
			this.gameOver = key
			this.setNewScreen('GameOver', nextScreen)
		},
		setNewScreen(screen: GameScreens, nextScreen?: GameScreens) {
			if (this._nextScreen) {
				this.currentScreen = this._nextScreen
				this._nextScreen = undefined
				return
			}
			this.currentScreen = screen
			this._nextScreen = nextScreen
		},
	},
})
