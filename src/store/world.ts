import { defineStore } from 'pinia'
import { IDestination, DestinationUnlock } from '@/globals/destination'
import type { TDestinations } from '@/globals/destination'

export const useWorldStore = defineStore('world', {
	state: () => ({
		day: 1,

		uniqueEncounters: <string[]>[],
		unlockedDestinations: 0,

		currentDestination: <undefined | TDestinations>undefined,
	}),

	getters: {
		getDestinationDanger: (store) => {
			const lookup = ['None', 'Low', 'Medium', 'High']
			return (dest: IDestination) => {
				return lookup[dest.Danger]
			}
		},
		getDestinationSupplies: (store) => {
			const lookup = ['None', 'Varied', 'Food']
			return (dest: IDestination) => {
				return lookup[dest.Supplies]
			}
		},
		isDestinationUnlocked: (store) => {
			return (dest: IDestination) => {
				if (!dest.Unlock) return true

				if (Array.isArray(dest.Unlock)) {
					return dest.Unlock.every((unlock) => store.unlockedDestinations & unlock)
				}
				return !!(store.unlockedDestinations & dest.Unlock)
			}
		},
	},

	actions: {
		unlockDestination(unlock: DestinationUnlock | DestinationUnlock[]) {
			if (Array.isArray(unlock)) {
				unlock.forEach((u) => (this.unlockedDestinations |= u))
			} else {
				this.unlockedDestinations |= unlock
			}
		},
		lockDestination(unlock: DestinationUnlock | DestinationUnlock[]) {
			if (Array.isArray(unlock)) {
				unlock.forEach((u) => (this.unlockedDestinations &= ~u))
			} else {
				this.unlockedDestinations &= ~unlock
			}
		},
	},
})
