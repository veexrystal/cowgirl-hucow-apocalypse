import { defineStore } from 'pinia'
import { CowGirlFriendlyString, CreateCowGirl, TCowGirl } from '@/globals/cowgirl'
import {
	ISimpleEncounter,
	IFightEncounter,
	IEncounterResult,
	IEncounterOptionClick,
	ENCOUNTERS,
	IEncounterOptionRoll,
	IEncounterOption,
} from '@/globals/encounters'
import { $grabGlobalText, $translateText } from '@/globals'
import { useGlobalStore } from './global'
import { usePlayerStore } from './player'
import { useWorldStore } from './world'
import { TDestinations } from '@/globals/destination'

export const useEncounterStore = defineStore('encounter', {
	state: () => ({
		visible: false,
		tabData: {
			title: '',
			content: <string[]>[],
		},
		data: <ISimpleEncounter | IFightEncounter>{},

		__isSet: false,
	}),

	getters: {
		playerHitText: (state) => {
			return (damage: number) => {
				return $translateText(`You hit the $translation:info.type$ for ${damage} damage`)
			}
		},
		playerMissText: (state) => {
			return $translateText(
				`You try as much as you can to hit the $translation:info.type$. But it just manages to avoid your blow.`
			)
		},
		enemyHitText: (state) => {
			return (damage: number) => {
				return $translateText(
					`The $translation:info.type$ takes a swing at you. You try to avoid it but it manages to slip by your defenses and hits you square in the body. You lose ${damage} health.`
				)
			}
		},
		enemyMissText: (state) => {
			return $translateText(
				`The $translation:info.type$ takes a swing at you. You try to avoid and do so! It's slow lumbering body easy to predict`
			)
		},

		createInfectionProgress() {
			return (skip_transition = false): ISimpleEncounter => {
				const playerStore = usePlayerStore()
				const globalStore = useGlobalStore()
				return {
					title: 'Infection Increases',
					content: {
						DEFAULT: globalStore.cheats.stopInfectionProgress
							? $grabGlobalText('cheats.stopinfectionprocess')
							: playerStore.infectionProgress,
					},
					options: [
						{
							click: ({ emit }) => {
								emit('hide-encounter')
								if (skip_transition) return
								emit('show-transition')
							},
							title: 'End',
						},
					],
				}
			}
		},
		createDummyEncounter() {
			return (): ISimpleEncounter => {
				return {
					title: 'dummy',
					content: {
						DEFAULT: 'dummy',
					},
					options: [
						{
							title: 'dummy',
							click: ({ emit }) => emit('finish-day'),
						},
					],
				}
			}
		},
		createExplorationEncounter() {
			const dummy = this.createDummyEncounter
			return (dest: TDestinations): ISimpleEncounter => {
				const worldStore = useWorldStore()

				const unique = Object.values(ENCOUNTERS[dest].UNIQUE || {}).filter(
					(x) => !worldStore.uniqueEncounters.includes(x.title)
				)
				const encounters = [...unique, ...ENCOUNTERS[dest].MUNDANE].filter((e) => e.reachable !== false)

				if (encounters.length === 0) {
					console.error('Player has exhausted all encounters. This should not happen.')
					return dummy()
				}

				const selection = encounters[Math.floor(Math.random() * encounters.length)]
				if (ENCOUNTERS[dest].UNIQUE?.find((e) => e.title === selection.title))
					worldStore.uniqueEncounters.push(selection.title)

				return selection
			}
		},
		createFightEncounter() {
			return (
				type?: TCowGirl,
				winText?: string,
				rewards?: IEncounterResult[],
				onWin?: (options: IEncounterOptionClick) => any,
				onLose?: (options: IEncounterOptionClick) => any
			) => {
				const cow = CreateCowGirl(type)

				return {
					title: `An enemy ${CowGirlFriendlyString[cow.type]} confronts you!`,
					content: {
						DEFAULT: '',
						WINTEXT: winText ?? undefined,
					},
					cowgirl: cow,
					rewards,
				}
			}
		},
		createSubEncounterSimple() {
			return (text: string, options?: (IEncounterOption | IEncounterOptionRoll)[]): ISimpleEncounter => {
				return {
					title: this.data.title,
					content: {
						...this.data.content,
						DEFAULT: text,
					},
					options: options ?? [
						{
							title: 'End',
							click: ({ emit }) => emit('finish-day'),
						},
					],
				}
			}
		},
		createSubEncounterFight() {
			return (text: string, onClick: (option: IEncounterOptionClick) => any): ISimpleEncounter => {
				return {
					title: this.data.title,
					content: {
						...this.data.content,
						DEFAULT: text,
					},
					options: [
						{
							title: 'Fight!',
							click: onClick,
						},
					],
				}
			}
		},

		createLoseFightEncounter() {
			return () => {
				const worldStore = useWorldStore()

				if (worldStore.currentDestination === 'Hospital') {
					return {
						title: this.data.title,
						content: {
							DEFAULT: `The Enemy lands one solid hit on you and you fall to the ground. You can hear her and the other moo's slowly approach you...`,
						},
						options: [
							{
								title: 'They are coming...',
								click: () => {
									const globalStore = useGlobalStore()
									globalStore.setGameOver('LOSEHOSPITAL')
								},
							},
						],
					}
				}

				const playerStore = usePlayerStore()
				const cow = Object.assign((this.data as IFightEncounter).cowgirl, {})

				playerStore.health = Math.floor(playerStore.maxHealth / 2)

				playerStore.setInfection(cow.type)
				const doEncounter = playerStore.increaseInfection()

				return {
					title: this.data.title,
					content: {
						DEFAULT: $grabGlobalText('cowgirl.' + cow.type + '.losetext'),
					},
					options: [
						{
							title: 'Change',
							click: ({ store }) => {
								if (doEncounter) {
									if (playerStore.infection <= 3) {
										return store.setEncounter(store.createInfectionProgress(true))
									} else {
										// return TEXT.ENDINGS
									}
								}
							},
						},
					],
				} as ISimpleEncounter
			}
		},
		createWinFightEncounter() {
			return (options?: (IEncounterOption | IEncounterOptionRoll)[]) => {
				const cow = Object.assign((this.data as IFightEncounter).cowgirl, {})

				let winText = this.data.content.WINTEXT
					? $translateText(this.data.content.WINTEXT as string)
					: $grabGlobalText('cowgirl.' + cow.type + '.losetext')

				if (this.data.rewards) {
					const playerStore = usePlayerStore()
					winText += '\n\n' + playerStore.giveRewardOrPunish('reward', this.data.rewards)
				}

				return {
					title: this.data.title,
					content: {
						DEFAULT: winText,
					},
					options: options ?? [
						{
							title: 'End',
							click: ({ emit }) => emit('finish-day'),
						},
					],
				} as ISimpleEncounter
			}
		},
	},

	actions: {
		showEncounter() {
			this.visible = true
		},
		hideEncounter() {
			this.$reset()
			this.visible = false

			const worldStore = useWorldStore()
			worldStore.currentDestination = undefined
		},

		setEncounter(encounter: ISimpleEncounter | IFightEncounter, reset = false) {
			this.data = encounter

			if (reset || !this.__isSet) {
				this.tabData.title = $translateText(encounter.title)
			}

			this.tabData.content = []

			this.pushEncounterText(encounter.content.DEFAULT)
			if ('options' in encounter && encounter.options === 'none' && encounter.rewards) {
				const playerStore = usePlayerStore()
				this.tabData.content.push('\n\n' + playerStore.giveRewardOrPunish('reward', encounter.rewards))
			}

			this.__isSet = true
		},
		clearEncounterText() {
			this.tabData.content = []
		},

		// tab content //
		pushEncounterText(text: string) {
			if (!this.visible) return
			this.tabData.content.push($translateText(text))
		},
	},
})
