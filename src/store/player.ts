import { defineStore } from 'pinia'
import { TCowGirl, ICowGirl } from '../globals/cowgirl'
import { TProjects, GENDER } from '../globals/player'
import { TSkill } from '../globals/entity'
import { TEXT } from '../globals/text'
import { IEncounterResult } from '../globals/encounters'
import { $grabGlobalText } from '../globals'
import { ENDINGS, TEnding } from '@/globals/endings'
import { useWorldStore } from './world'
import { useGlobalStore } from './global'

export const usePlayerStore = defineStore('player', {
	state: () => ({
		name: '',
		gender: <GENDER>GENDER.MALE,

		stats: {
			unassigned: 4,

			strength: 1,
			intelligence: 1,
			agility: 1,
			stamina: 1,
			perception: 1,
		},

		infectionType: <TCowGirl>'mooerudder', // NOTE: udder kink is enabled by default

		food: 3,
		foodToConsume: 1,
		__oldFood: 3,

		experience: 0,
		maxExperience: 20,
		health: 11,

		materials: 0,
		projectBonus: {
			combat: 0,
			defense: 0,
			utility: 0,
		},

		skillPoints: 0,
		skillBonus: {
			strength: 0,
			intelligence: 0,
			agility: 0,
			stamina: 0,
			perception: 0,
		},

		infection: 0,
	}),

	getters: {
		maxHealth: (state) => {
			return 11 + state.skillBonus.stamina * 2
		},
		isPlayerMale: (state) => state.gender === GENDER.MALE,
		genderAsString: (state) => (state.gender === 0 ? 'Male' : 'Female'),

		justRanOutOfFood: (state) => state.food === 0 && state.__oldFood === 1,

		getCalculatedSkill(state) {
			return (skill: TSkill) => {
				return state.stats[skill] + state.skillBonus[skill]
			}
		},

		infectionDescription: (state) => {
			if (state.infection === 0) return $grabGlobalText('infection.basedescription')

			return $grabGlobalText(
				`infection.${state.infectionType}.${state.gender === 0 ? 'Male' : 'Female'}.description.${
					state.infection - 1
				}`
			)
		},
		infectionProgress: (state) => {
			if (state.infection === 0) return ''

			return $grabGlobalText(
				`infection.${state.infectionType}.${state.gender === 0 ? 'Male' : 'Female'}.progress.${
					state.infection - 1
				}`
			)
		},

		infectionEnding: (state) => {
			const globalStore = useGlobalStore()
			const infection = TEXT.INFECTION[state.infectionType.toUpperCase()]
			const endingNum =
				globalStore.gameMode === 'hucow' && 'HUCOWENDING' in infection
					? infection.HUCOWENDING
					: infection.ENDING
			return Object.keys(ENDINGS).find((name) => ENDINGS[name as TEnding].ENDING === endingNum)! as TEnding
		},
		timeLimitEnding: (state) => {
			let ending: TEnding = 'TIMELIMIT0'
			switch (state.infection) {
				case 1:
					ending = 'TIMELIMIT1'
					break
				case 2:
					ending = 'TIMELIMIT2'
					break
				case 3:
					ending = 'TIMELIMIT3'
					break
			}
			return ending
		},
		escapeEnding: (state) => {
			let ending: TEnding = 'ESCAPE0'
			switch (state.infection) {
				case 1:
					ending = 'ESCAPE1'
					break
				case 2:
				case 3:
					ending = 'ESCAPE2'
					break
			}
			return ending
		},
	},

	actions: {
		setInfection(infection: TCowGirl) {
			this.infectionType = infection
		},
		increaseInfection() {
			if (this.infection >= 3) return false
			this.infection++
			return true
		},

		incrementStat(name: TSkill) {
			if (this.stats.unassigned < 1) return
			this.stats[name]++
			this.stats.unassigned--
		},
		decrementStat(name: TSkill) {
			if (this.stats[name] < 2) return
			this.stats[name]--
			this.stats.unassigned++
		},

		upgradeSkill(skill: TSkill) {
			if (this.skillPoints < 1) return
			this.skillBonus[skill]++
			this.skillPoints--
		},
		upgradeProject(project: TProjects) {
			if (this.materials < 1) return
			const cost = TEXT.PROJECTS[project.toUpperCase()].COST as number
			if (this.materials < cost) return
			this.projectBonus[project]++
			this.materials -= cost
		},

		consumeFood() {
			this.__oldFood = this.food
			if (this.food > 0) {
				this.food = this.food - this.foodToConsume
				if (this.health <= this.maxHealth) {
					this.health += 1
				}
			}
		},
		gainExperience(amt: number) {
			this.experience += amt
			while (this.experience >= this.maxExperience) {
				this.experience -= this.maxExperience
				this.skillPoints += 1
			}
		},

		giveRewardOrPunish(type: 'reward' | 'punish', rewards: IEncounterResult[]) {
			let rewardsTemp: string[] = []
			let rewardsEnd = '.'

			const worldStore = useWorldStore()

			rewards.forEach((reward) => {
				if (typeof reward === 'object' && 'amount' in reward) {
					const amt = reward.amount * (type === 'reward' ? 1 : -1)
					switch (reward.reward) {
						case 'none':
							{
							}
							break

						case 'experience':
							{
								if (this.experience + amt >= this.maxExperience) {
									rewardsEnd = `.\n\nYou have gained a skillpoint!`
								}
								this.gainExperience(amt)
								rewardsTemp.push(
									`${reward.amount} ${$grabGlobalText('rewards.friendlytext.' + reward.reward)}`
								)
							}
							break

						default:
							{
								// nifty shortcut since all reward name exists in playerStore
								this[reward.reward] += amt
								rewardsTemp.push(
									`${reward.amount} ${$grabGlobalText('rewards.friendlytext.' + reward.reward)}`
								)
							}
							break
					}
				} else {
					worldStore.unlockedDestinations |= reward
				}
			})

			if (rewardsTemp.length === 1)
				return `You ${type === 'reward' ? 'gained' : 'lost'} ${rewardsTemp[0]}${rewardsEnd}`
			else
				return `You gained ${rewardsTemp.slice(0, -1).join(', ')} and ${
					rewardsTemp[rewardsTemp.length - 1]
				}${rewardsEnd}`
		},
	},
})
